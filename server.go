package main

import (
	"crypto/rand"
	"fmt"
	"log"
	"net/mail"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"
)

type Server struct {
	Engine       *gin.Engine
	UserHandler  UserHandler
	AdminHandler AdminHandler
}

func NewServer(config Config) Server {
	if config.Logging.Writer == nil {
		config.Logging.Writer = log.Writer()
	} else {
		log.SetOutput(config.Logging.Writer)
	}

	r, t := configureGin(config.Secret, config.Logging.Writer, map[string]Validator{
		"email": {
			Func: func(fl validator.FieldLevel) bool {
				field, ok := fl.Field().Interface().(string)
				_, err := mail.ParseAddress(field)

				return ok && err == nil
			},
			Message: "Invalid e-mail address",
		},
		"password": {
			Func: func(fl validator.FieldLevel) bool {
				field, ok := fl.Field().Interface().(string)
				return ok && len(field) >= config.Routing.MinimumPasswordLength
			},
			Message: fmt.Sprintf("Password must be at least %d characters long", config.Routing.MinimumPasswordLength),
		},
	})

	db := openDatabase(config.DatabasePath, config.Logging)

	base := BaseHandler{
		Translator: t,
		Config:     config,
		DB:         db,
	}

	s := Server{
		Engine: r,

		UserHandler: UserHandler{
			BaseHandler: base,
		},
		AdminHandler: AdminHandler{
			BaseHandler: base,
		},
	}

	r.Use(gin.CustomRecoveryWithWriter(config.Logging.Writer, base.recovery))
	r.NoRoute(base.show404)

	return s
}

func secret() []byte {
	var secretBytes []byte
	if !viper.IsSet("cookie.secret") {
		log.Print("WARNING: cookie.secret not set, using random bytes. This means cookies won't persist between runs")
		secretBytes = make([]byte, 512)
		_, err := rand.Read(secretBytes)
		if err != nil {
			panic(err)
		}
	} else {
		secret := viper.GetString("cookie.secret")
		secretBytes = []byte(secret)
	}

	return secretBytes
}

func userServer(config Config) {
	g := NewServer(config)
	g.UserHandler.playlistStreamer = NewPlaylistStreamer()
	g.Engine.Use(g.UserHandler.checkAuth)
	g.UserHandler.registerRoutes(g.Engine)

	go g.UserHandler.streamEntries()

	g.Engine.Run(":8080")
}

func adminServer(config Config) {
	g := NewServer(config)
	g.AdminHandler.registerRoutes(g.Engine, config.Admins)
	g.Engine.Run(":8008")
}
