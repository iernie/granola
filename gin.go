package main

import (
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"path/filepath"
	"runtime/debug"
	"strings"
	"time"

	method "github.com/bu/gin-method-override"
	"github.com/dustin/go-humanize"
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	translations "github.com/go-playground/validator/v10/translations/en"
	"github.com/spf13/cast"
)

type Validator struct {
	Func    validator.Func
	Message string
}

func createRender() multitemplate.Renderer {
	r := multitemplate.NewRenderer()

	addTemplate := func(templateName string, templates ...string) {
		r.AddFromFilesFuncs(templateName, template.FuncMap{
			"granolaVersion": func() string {
				info, _ := debug.ReadBuildInfo()
				return info.Main.Version
			},
			"templateName": func() string { return templateName },
			"add":          func(a int, b int) int { return a + b },
			"sub":          func(a int, b int) int { return a - b },
			"int":          func(a interface{}) int { return cast.ToInt(a) },
			"iterate": func(from uint, to uint) []uint {
				items := []uint{}
				if from > to {
					for i := from; i >= to; i-- {
						items = append(items, i)
					}
				} else {
					for i := from; i <= to; i++ {
						items = append(items, i)
					}
				}

				return items
			},
			"humanize": func(what string, value any) any {
				if value != nil {
					switch what {
					case "time":
						return humanize.Time(value.(time.Time))
					}
				}

				return value
			},
		}, templates...)
	}

	addTemplates := func(pattern string, requiredTemplates ...string) {
		matches, err := filepath.Glob(pattern)
		if err != nil {
			panic(err)
		}

		for _, f := range matches {
			if strings.HasPrefix(filepath.Base(f), "_") {
				continue
			}

			templateName := strings.TrimSuffix(strings.TrimPrefix(f, "templates/"), ".html")
			baseTemplate := "templates/_base.html"
			requiredTemplates = append(requiredTemplates, baseTemplate)
			requiredTemplates = append(requiredTemplates, f)
			addTemplate(templateName, requiredTemplates...)
		}
	}

	addTemplate("error", "templates/_base.html", "templates/error.html")
	addTemplate("entry-preview", "templates/entry-preview.html")
	addTemplates("templates/user/*.html", "templates/user/_base.html")
	addTemplates("templates/user/voting/*.html",
		"templates/user/_base.html",
		"templates/user/voting/_checkout.html",
		"templates/user/voting/_current-vote.html",
		"templates/user/voting/_entry-frame.html",
		"templates/user/voting/_place-vote.html",
		"templates/user/voting/_playlist-navigation.html")
	addTemplates("templates/admin/*.html", "templates/admin/_base.html")

	return r
}

func configureStaticFiles(r *gin.Engine) {
	files, err := ioutil.ReadDir("./static")
	if err != nil {
		panic(err)
	}

	for _, f := range files {
		if f.IsDir() {
			r.Static("/"+f.Name(), "./static/"+f.Name())
		} else {
			r.StaticFile("/"+f.Name(), "./static/"+f.Name())
		}
	}
}

func configureValidation(validators map[string]Validator) ut.Translator {
	if validation, ok := binding.Validator.Engine().(*validator.Validate); ok {
		englishLocaleTranslator := en.New()
		universalTranslator := ut.New(englishLocaleTranslator, englishLocaleTranslator)
		// this is usually known or extracted from http 'Accept-Language' header
		// also see uni.FindTranslator(...)
		translator, found := universalTranslator.GetTranslator("en")
		if !found {
			log.Fatal("'en' translator not found")
		}
		translations.RegisterDefaultTranslations(validation, translator)

		for tag, v := range validators {
			f := v.Func
			msg := v.Message

			if err := validation.RegisterValidation(tag, f); err != nil {
				log.Fatal(fmt.Printf("Could not register validator for '%s'.", tag))
			}

			if msg == "" {
				continue
			}

			registerFn := func(ut ut.Translator) error {
				return ut.Add(tag, msg, true)
			}

			translationFn := func(ut ut.Translator, fe validator.FieldError) string {
				return msg
			}

			validation.RegisterTranslation(tag, translator, registerFn, translationFn)
		}

		return translator
	} else {
		log.Fatal("Could not configure Gin validator")
	}

	return nil
}

func configureGin(secret []byte, log io.Writer, validators map[string]Validator) (*gin.Engine, ut.Translator) {
	r := gin.New()
	r.Use(gin.LoggerWithWriter(log))

	store := cookie.NewStore(secret)
	r.Use(sessions.Sessions("session", store))
	r.Use(method.ProcessMethodOverride(r))

	r.MaxMultipartMemory = 128 << 20 // 128 MiB
	r.HTMLRender = createRender()

	configureStaticFiles(r)
	t := configureValidation(validators)

	return r, t
}
