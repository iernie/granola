package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"regexp"
	"strings"
	"testing"

	"granola/models"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gorm.io/gorm"
)

type TestRouter struct {
	Server Server
	DB     *gorm.DB
}

type TestResponseRecorder struct {
	*httptest.ResponseRecorder
	closeChannel chan bool
}

type TestResponse struct {
	ContentType string
	Location    string
	StatusCode  int
	Body        []byte
	Cookies     []string
}

func (response *TestResponse) BodyString() string {
	return string(response.Body)
}

func (response *TestResponse) BodyJSON() gin.H {
	if !strings.Contains(response.ContentType, "application/json") {
		panic(fmt.Errorf("Content-Type is not JSON: %s", response.ContentType))
	}

	var h gin.H
	if err := json.Unmarshal(response.Body, &h); err != nil {
		s := response.BodyString()
		log.Printf("Error converting JSON. %v %s", err, s)
	}

	return h
}

func (r *TestResponseRecorder) CloseNotify() <-chan bool {
	return r.closeChannel
}

func (r *TestResponseRecorder) closeClient() {
	r.closeChannel <- true
}

func CreateTestResponseRecorder() *TestResponseRecorder {
	return &TestResponseRecorder{
		httptest.NewRecorder(),
		make(chan bool, 1),
	}
}

func (r *TestRouter) requestHTML(method string, endpoint string, cookies []string) TestResponse {
	req, _ := http.NewRequest(method, endpoint, nil)
	req.Header.Set("Accept", "text/html")

	for _, cookie := range cookies {
		req.Header.Set("Cookie", cookie)
	}

	w := httptest.NewRecorder()
	r.Server.Engine.ServeHTTP(w, req)
	responseData, _ := ioutil.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		Cookies:     w.Header().Values("Set-Cookie"),
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) uploadFileWithCookies(method string, endpoint string, data []byte, filename string, cookies []string) TestResponse {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	contentType := writer.FormDataContentType()
	part, _ := writer.CreateFormFile("file", filename)
	part.Write(data)
	writer.Close()
	return r.requestApiWithCookies(method, endpoint, contentType, body, cookies)
}

func (r *TestRouter) requestJsonApiWithCookies(method string, endpoint string, data []byte, cookies []string) TestResponse {
	body := bytes.NewBuffer(data)
	contentType := "application/json; charset=UTF-8"
	return r.requestApiWithCookies(method, endpoint, contentType, body, cookies)
}

func (r *TestRouter) requestApiWithCookies(method string, endpoint string, contentType string, body io.Reader, cookies []string) TestResponse {
	req, _ := http.NewRequest(method, endpoint, body)
	req.Header.Set("Content-Type", contentType)

	for _, cookie := range cookies {
		req.Header.Set("Cookie", cookie)
	}

	w := CreateTestResponseRecorder()
	r.Server.Engine.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		Cookies:     w.Header().Values("Set-Cookie"),
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) fetchAPIEndpoint(method string, endpoint string, data []byte) TestResponse {
	return r.requestJsonApiWithCookies(method, endpoint, data, []string{})
}

func setupTestRouterCommon(t *testing.T, config Config) TestRouter {
	config.Routing.MinimumPasswordLength = 5
	config.RateLimit.Register = ^uint32(0)
	config.RateLimit.Login = ^uint32(0)
	if !testing.Verbose() {
		config.Logging.Writer = io.Discard
		config.Logging.Level = Silent
	}
	gin.SetMode(gin.TestMode)
	s := NewServer(config)

	// concurrent database-access for in-memory databases doesn't work well
	// in sqlite, so let's limit the open connections.
	db := s.UserHandler.DB
	sql, _ := db.DB()
	sql.SetMaxOpenConns(1)

	t.Cleanup(func() {
		sql, _ := db.DB()
		sql.Close()
		db = nil
	})

	return TestRouter{
		Server: s,
		DB:     db,
	}
}

func NewTestRouterUser(t *testing.T) TestRouter {
	config := Config{
		Secret:       []byte("not-very-secret"),
		DatabasePath: ":memory:",
	}
	r := setupTestRouterCommon(t, config)
	r.Server.Engine.Use(r.Server.UserHandler.checkAuth)
	r.Server.UserHandler.registerRoutes(r.Server.Engine)
	return r
}

func NewTestRouterAdmin(t *testing.T) TestRouter {
	config := Config{
		Secret:       []byte("not-very-secret"),
		DatabasePath: ":memory:",
		Admins:       map[string]string{"admin": "password"},
	}
	r := setupTestRouterCommon(t, config)
	r.Server.AdminHandler.registerRoutes(r.Server.Engine, config.Admins)
	return r
}

func (r *TestRouter) registerUser(email string, handle string, invite string, password string) TestResponse {
	str, _ := json.Marshal(gin.H{
		"email":    email,
		"handle":   handle,
		"invite":   invite,
		"password": password,
	})
	return r.fetchAPIEndpoint("POST", "/register", []byte(str))
}

func (r *TestRouter) loginUser(email string, password string) TestResponse {
	str, _ := json.Marshal(gin.H{
		"email":    email,
		"password": password,
	})
	return r.requestJsonApiWithCookies("POST", "/login", []byte(str), []string{})
}

func (r *TestRouter) createUserEntry(compo int, title string, author string, cookies []string) TestResponse {
	str, _ := json.Marshal(gin.H{
		"compo":  compo,
		"title":  title,
		"author": author,
		"notes":  "**be strong**",
	})
	return r.requestJsonApiWithCookies("POST", "/entries", []byte(str), cookies)
}

func (r *TestRouter) createUserTestEntry(t *testing.T, cookies []string) {
	response := r.createUserEntry(1, "test-entry", "test-author", cookies)
	assert.Equal(t, http.StatusCreated, response.StatusCode)

	json := response.BodyJSON()
	assert.Equal(t, "test-entry", json["title"])
	assert.Equal(t, "test-author", json["author"])
	assert.Equal(t, "**be strong**", json["notes"])
	assert.Regexp(t, regexp.MustCompile(`^/compos/[1-9]\d*$`), json["compo"])
	assert.Regexp(t, regexp.MustCompile(`^/entries/[1-9]\d*$`), json["id"])
	assert.Regexp(t, regexp.MustCompile(`/users/[1-9]\d*$`), json["user"])
	assert.Equal(t, false, json["locked"])
	assert.Regexp(t, regexp.MustCompile(`^/entries/[1-9]\d*/preview$`), json["preview"].(map[string]any)["id"])
}

func (r *TestRouter) updateUserEntry(entry int, title string, author string, platform string, notes string, cookies []string) TestResponse {
	str, _ := json.Marshal(gin.H{
		"title":    title,
		"author":   author,
		"platform": platform,
		"notes":    notes,
	})
	return r.requestJsonApiWithCookies("PUT", fmt.Sprintf("/entries/%d", entry), []byte(str), cookies)
}

func (r *TestRouter) fetchAdminAPIEndpoint(method string, endpoint string, data io.Reader) TestResponse {
	req, _ := http.NewRequest(method, endpoint, data)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	w := CreateTestResponseRecorder()
	r.Server.Engine.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) adminCreateCompo(name string, multiPlatform bool, locked bool) TestResponse {
	str, _ := json.Marshal(gin.H{"name": name, "multiplatform": multiPlatform, "locked": locked})
	return r.fetchAdminAPIEndpoint("POST", "/compos", bytes.NewBuffer(str))
}

func (r *TestRouter) createCompo(t *testing.T, name string, multiPlatform bool, locked bool) {
	_, err := models.NewCompo(r.DB, name, multiPlatform, locked)
	assert.Nil(t, err)
}

func (r *TestRouter) createTestCompo(t *testing.T) {
	r.createCompo(t, "test compo", true, false)
}

func (r *TestRouter) updateCompo(compo int, obj gin.H) TestResponse {
	str, _ := json.Marshal(obj)
	return r.fetchAdminAPIEndpoint("PUT", fmt.Sprintf("/compos/%d", compo), bytes.NewBuffer([]byte(str)))
}

func (r *TestRouter) adminCreateInvite(key string) TestResponse {
	str, _ := json.Marshal(gin.H{"key": key})
	return r.fetchAdminAPIEndpoint("POST", "/invites", bytes.NewBuffer(str))
}

func (r *TestRouter) createInvite(t *testing.T, key string) {
	_, err := models.NewInvite(r.DB, key)
	assert.Nil(t, err)
}

func (r *TestRouter) createTestUser(t *testing.T) {
	r.createInvite(t, "test-invite")
	r.registerUser("test-user@example.com", "test.user", "test-invite", "test-password")
}

func (r *TestRouter) lockEntry(entryID int) {
	entry := models.Entry{}
	result := r.DB.First(&entry, entryID)
	if result.Error != nil {
		panic(result.Error)
	}
	entry.Locked = true

	result = r.DB.Save(&entry)
	if result.Error != nil {
		panic(result.Error)
	}
}

func (r *TestRouter) createEntry(compo int, title string, author string) TestResponse {
	str, _ := json.Marshal(gin.H{
		"title":  title,
		"author": author,
	})
	return r.fetchAdminAPIEndpoint("POST", fmt.Sprintf("/compos/%d/entries", compo), bytes.NewBuffer([]byte(str)))
}

func (r *TestRouter) createTestEntry(t *testing.T) {
	response := r.createEntry(1, "test-entry", "test-author")
	data := response.BodyJSON()
	assert.Equal(t, http.StatusCreated, response.StatusCode)
	assert.Equal(t, "/entries/1", data["id"])
	assert.Equal(t, "/compos/1", data["compo"])
	assert.Equal(t, "test-author", data["author"])
	assert.Equal(t, "test-entry", data["title"])
	assert.Equal(t, false, data["locked"])
	assert.Equal(t, map[string]any{"id": "/entries/1/preview"}, data["preview"])
}

func (r *TestRouter) updateEntry(entry int, obj gin.H) TestResponse {
	str, _ := json.Marshal(obj)
	return r.fetchAdminAPIEndpoint("PUT", fmt.Sprintf("/entries/%d", entry), bytes.NewBuffer([]byte(str)))
}

func (r *TestRouter) deleteEntry(entry int) TestResponse {
	return r.fetchAdminAPIEndpoint("DELETE", fmt.Sprintf("/entries/%d", entry), nil)
}

func (r *TestRouter) createUpload(entry int, filename string, data []byte) TestResponse {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, _ := writer.CreateFormFile("file", filename)
	part.Write(data)
	writer.Close()
	endpoint := fmt.Sprintf("/entries/%d/uploads", entry)

	req, _ := http.NewRequest("POST", endpoint, body)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Content-Type", writer.FormDataContentType())
	w := CreateTestResponseRecorder()
	r.Server.Engine.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	return TestResponse{
		StatusCode:  w.Code,
		Body:        responseData,
		ContentType: w.Header().Get("Content-Type"),
		Location:    w.Header().Get("Location"),
	}
}

func (r *TestRouter) createPlaylist(t *testing.T, name string, entries []int) *models.Playlist {
	playlist := models.Playlist{Name: name, Entries: []models.PlaylistEntry{}}
	for _, entryID := range entries {
		e := models.PlaylistEntry{EntryID: uint(entryID)}
		playlist.Entries = append(playlist.Entries, e)
	}
	err := r.DB.Create(&playlist).Error
	assert.Nil(t, err)

	return &playlist
}
