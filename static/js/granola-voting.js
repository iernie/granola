(function() {
  class Voting {
    votingContainer;

    constructor(votingContainer) {
      if (votingContainer === null || votingContainer === undefined) {
        throw 'Voting container is required';
      }

      this.votingContainer = votingContainer;
    }

    setUpEntrySelection() {
      const self = this;
      const votingContainer = this.votingContainer;
      const firstEntry = votingContainer.querySelector('.entry:first-of-type');
      this.setPreviousEntry(votingContainer.querySelector('input[name="entry-selector"]:checked'));

      votingContainer.querySelectorAll('input[name="entry-selector"]').forEach(function(radio) {
        // When the last entry is selected, add the 'wrap-around' class to
        // the first entry so it can be styled differently. This is
        // required because Firefox doesn't support the :has() selector.
        radio.addEventListener('change', function(event) {
          self.setPreviousEntry(this);

          // The radio button is followed by an entry, so we need to check
          // that the entry isn't followed by any other elements.
          const isLast = this.nextElementSibling !== null
                      && this.nextElementSibling.nextElementSibling === null;
          if (isLast) {
            firstEntry.classList.add('wrap-around');
          } else {
            firstEntry.classList.remove('wrap-around');
          }
        });
      });
    }

    setPreviousEntry(checkedRadioButton) {
      this.votingContainer.querySelectorAll('.entry').forEach(function(entry) {
        entry.classList.remove('previous');
      });

      if (checkedRadioButton && checkedRadioButton.previousElementSibling)  {
        checkedRadioButton.previousElementSibling.classList.add('previous');
      }
    }

    cloneTemplate() {
      if ('content' in document.createElement('template')) {
        const template = document.getElementById('voting-template');
        return template.content.firstElementChild.cloneNode(true);
      }

      return null;
    }

    initializeEventSourceStream() {
      const self = this;

      if (!document.getElementById('voting').classList.contains('live')) {
        // The event source is only needed for live-voting
        return null;
      }

      const stream = new EventSource('/voting/entries');
      window.addEventListener('beforeunload', function() {
        stream.close();
      });
      stream.addEventListener('error', function(e) {
        stream.close();
        console.error(e);
        self.displayError('An error ocurred. Please reload the page.')
      });

      var lastVotingEntry = null;
      stream.addEventListener('message', function(e) {
        const votingEntry = JSON.parse(e.data)

        if (!votingEntry) {
          return;
        }

        if (lastVotingEntry != null
            && votingEntry.id === lastVotingEntry.id
            && votingEntry.playlist.liveVotingClosedAt === lastVotingEntry.playlist.liveVotingClosedAt) {
          return;
        }

        if (votingEntry.playlist.liveVotingClosedAt) {
          self.displayError(`${votingEntry.playlist.name} is closed for voting. Please check back later.`)
          stream.close();
        } else {
          self.handleReceivedVotingEntry(votingEntry);
          lastVotingEntry = votingEntry;
        }
      });

      return stream;
    }

    handleReceivedVotingEntry(votingEntry) {
      console.log(votingEntry);

      const existingEntrySelector = this.votingContainer.querySelector(`input[name="entry-selector"][value="${votingEntry.id}"]`)
      if (existingEntrySelector) {
        existingEntrySelector.checked = true;
        this.setPreviousEntry(existingEntrySelector);
        return;
      }

      const clone = this.cloneTemplate();
      if (!clone) {
        console.error('Your browser does not support templates');
        this.votingContainer.innerHTML = 'Your browser does not support templates';
        return;
      }

      this.refreshEntry(clone, votingEntry);
      const entries = this.votingContainer.querySelector('.entries');
      const entry = entries.appendChild(clone);
      const radio = this.createEntrySelector(votingEntry);
      entry.parentNode.insertBefore(radio, entry);
      radio.checked = true;
    }

    createEntrySelector(votingEntry) {
      const radio = document.createElement('input');
      radio.setAttribute('type', 'radio');
      radio.setAttribute('id', `entry-${votingEntry.order}`);
      radio.setAttribute('name', 'entry-selector');
      radio.setAttribute('value', votingEntry.id);
      return radio;
    }

    refreshEntry(entry, votingEntry) {
      entry.querySelectorAll('.place-vote form').forEach(function(form) {
        form.setAttribute('action', votingEntry.votes.id)
      });

      entry.setAttribute('class', 'entry')
      entry.setAttribute('data-entry-id', votingEntry.id);
      entry.setAttribute('id', `entry-${votingEntry.order}`);
      entry.querySelector('iframe').setAttribute('src',  votingEntry.id);

      if (votingEntry.vote && votingEntry.vote.score > 0) {
        this.refreshVote(entry, votingEntry.vote);
      } else {
        currentVote.classList.add('hidden');
      }

      entry.querySelector('label.entry-frame').setAttribute('for', `entry-${votingEntry.order}`);
    }

    refreshVote(currentVoteContainer, vote) {
      const voteText = currentVoteContainer.querySelector('.vote-text');
      voteText.classList.remove('hidden');
      const time = voteText.querySelector('.time');
      time.setAttribute('datetime', vote.created.dateTime);
      time.innerHTML = vote.created.humanized;
      voteText.querySelector('.score').innerText = vote.score;

      currentVoteContainer.querySelectorAll('.place-vote .star').forEach(function(star) {
        star.classList.remove('selected');
      });

      currentVoteContainer.querySelector(`.place-vote .star-${vote.score}`).classList.add('selected');
    }

    setUpEventSourceStreamStatus(stream) {
      if (stream === null || stream === undefined) {
        return;
      }

      const status = document.querySelector('#voting .status');
      const updateStatus = function() {
        const state = (function() {
          switch (stream.readyState) {
            case EventSource.CONNECTING:
              return 'Connecting';
            case EventSource.OPEN:
              return 'Connected';
            case EventSource.CLOSED:
              return 'Disconnected';
          }

          return 'Unknown';
        })();

        status.innerHTML = state;
        const stateClass = state.toLowerCase();
        status.setAttribute('class', 'status status-' + stateClass)
      };

      setInterval(updateStatus, 100);
    }

    setUpVoteSubmission() {
      var self = this;
      this.votingContainer.querySelectorAll('.place-vote form').forEach(function(form) {
        form.addEventListener('submit', function(e) {
          e.preventDefault();

          const score = parseInt(this.querySelector('button[name="score"]').value, 10);

          let options = {
            method: this.getAttribute('method'),
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ score: score })
          }

          fetch(this.getAttribute('action'), options)
            .then(function(response) {
              return response.json()
            })
            .then(function(vote) {
              let parentNode = function(parent) {
                while (parent = parent.parentNode) {
                  if (parent.classList && parent.classList.contains('vote')) {
                    return parent;
                  }
                }

                return null;
              }(form);

              self.refreshVote(parentNode, vote);
            }).catch(function(error) {
              console.error(error);
            });

          return false;
        });
      });
    }

    displayError(message) {
      this.votingContainer.innerHTML = '';
      const p = document.createElement('p');
      p.innerText = message;
      this.votingContainer.parentNode.appendChild(p);
    }
  }

  document.addEventListener('DOMContentLoaded', function() {
    const votingContainer = document.querySelector('#voting .voting-container');
    if (votingContainer) {
      const voting = new Voting(votingContainer)
      voting.setUpEntrySelection();

      const stream = voting.initializeEventSourceStream();
      voting.setUpEventSourceStreamStatus(stream);
      voting.setUpVoteSubmission();
    }
  });
})();
