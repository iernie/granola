package main

import (
	"log"
	"time"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	. "granola/models"
)

func openDatabase(path string, logConfig LogConfig) *gorm.DB {
	gormConfig := gorm.Config{}
	ln := log.New(logConfig.Writer, "\r\n", log.LstdFlags)
	logLevel := logger.LogLevel(logConfig.Level)
	ignoreNotFound := logLevel == logger.Silent
	lc := logger.Config{
		SlowThreshold:             time.Second,
		LogLevel:                  logLevel,
		IgnoreRecordNotFoundError: ignoreNotFound,
		Colorful:                  true,
	}
	gormConfig.Logger = logger.New(ln, lc)

	db, err := gorm.Open(sqlite.Open(path), &gormConfig)
	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&User{})
	db.AutoMigrate(&Invite{})
	db.AutoMigrate(&Compo{})
	db.AutoMigrate(&Entry{})
	db.AutoMigrate(&Upload{})
	db.AutoMigrate(&Playlist{})
	db.AutoMigrate(&PlaylistEntry{})
	db.AutoMigrate(&Vote{})

	return db
}
