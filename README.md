# Granola

A minimal, modern competition system for [demoparties].

## Design goals

- Minimal: This means no accounting system, no plugins. Just what you need
  to run competitions at parties.
- Portable: Moving the compo server to a different machine should be a
  matter of moving a folder of configuration and data, and restarting the
  application.
- *Small* data: No enterprise database. Even the biggest of demoparties are
  nowhere near needing big-data type databases. We're using SQLite, because
  that should be enough.
- Short lived: A demo party typically takes place over a weekend. There
  should be no need to migrate database layouts etc. So we don't even try
  to get this stuff right.
- Highly customizable: No hard-coded HTML anywhere. Everything should be
  rendered from templates, which is part of the configuration.
- Easily accessible data: All data stored in the system should be easily
  accessible using REST APIs.
- Secure: Both the admin interface and all API end-points should be
  authenticated when exposing non-public information.

## Getting Started

In order to build, you need the [Go][golang] programming language
installed.

It's fairly simple:

```kbd
$ go run . server
Listening on :8080...
```

This brings up the main user-interface on port 8080.

There's also an admin-interface, that can be brought up separately on
port 8008, by running:

```kbd
$ go run . server-admin
Listening on :8008...
```


## Using Docker

By using Docker you do not need Go installed on your local machine, but
instead [Docker][docker]. This will download everything needed on first
run, but then be faster on restart.

```
$ cd .docker/
$ docker-compose up
Listening on :8080...
```

[demoparties]: https://en.wikipedia.org/wiki/Demoscene#Parties
[golang]: https://go.dev/dl/
[docker]: https://docs.docker.com/get-docker/
