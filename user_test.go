package main

import (
	"fmt"
	"net/http"
	"sync"
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRegisterValidation(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("user can't register with empty e-mail", func(t *testing.T) {
		response := r.registerUser("", "test-user", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Email is a required field", json["detail"])
	})

	t.Run("user can't register with empty handle", func(t *testing.T) {
		response := r.registerUser("test-user@example.com", "", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Handle is a required field", json["detail"])
	})

	t.Run("user can't register with empty invite key", func(t *testing.T) {
		response := r.registerUser("test-user@example.com", "test-user", "", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Invite is a required field", json["detail"])
	})

	t.Run("user can't register with empty password", func(t *testing.T) {
		response := r.registerUser("test-user@example.com", "test-user", "test-invite", "")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Password is a required field", json["detail"])
	})

	t.Run("user can't register with too short e-mail", func(t *testing.T) {
		response := r.registerUser("u", "test-user", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Invalid e-mail address", json["detail"])
	})

	t.Run("user can't register with invalid e-mail", func(t *testing.T) {
		response := r.registerUser("abc", "test-user", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Invalid e-mail address", json["detail"])
	})

	t.Run("user can't register with too short password", func(t *testing.T) {
		response := r.registerUser("test-user@example.com", "test-user", "test-invite", "p")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "Password must be at least 5 characters long", json["detail"])
	})

	t.Run("user can't register with invalid invitation", func(t *testing.T) {
		response := r.registerUser("test-user@example.com", "test-user", "invalid-invitation", "test-password")
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "invalid invitation", response.BodyJSON()["detail"])
	})
}

func TestRegister(t *testing.T) {
	r := NewTestRouterUser(t)

	r.createInvite(t, "test-invite")
	r.createInvite(t, "test-invite-2")

	t.Run("user 1 can register with invite key 1", func(t *testing.T) {
		response := r.registerUser("test-user@example.com", "test-user", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/users/1", json["id"])
		assert.Equal(t, "test-user@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"])
	})

	t.Run("user 1 can't register with invite key 2", func(t *testing.T) {
		response := r.registerUser("test-user@example.com", "test-user", "test-invite-2", "test-password")
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Contains(t, response.BodyJSON()["detail"], "users.email")
	})

	t.Run("user 2 can register with invite key 2", func(t *testing.T) {
		// Should succeed because `test-user-2` is not registered.
		response := r.registerUser("test-user-2@example.com", "test-user2", "test-invite-2", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/users/2", json["id"])
		assert.Equal(t, "test-user-2@example.com", json["email"])
		assert.Equal(t, "test-user2", json["handle"])
	})
}

func TestRegisterSpam(t *testing.T) {
	r := NewTestRouterUser(t)
	r.createInvite(t, "test-invite")

	N := 1000

	if testing.Short() {
		N = 100
	}

	t.Run("registering the same user multiple times should fail", func(t *testing.T) {
		var wg sync.WaitGroup
		var succeeded uint64

		wg.Add(N)
		for i := 0; i < N; i++ {
			i := i
			go func() {
				defer wg.Done()

				user := fmt.Sprintf("user-%d@example.com", i)
				response := r.registerUser(user, "test-user", "test-invite", "test-password")

				if response.StatusCode == http.StatusCreated {
					atomic.AddUint64(&succeeded, 1)
				} else {
					json := response.BodyJSON()
					assert.Equal(t, http.StatusBadRequest, response.StatusCode)
					assert.Equal(t, "invalid invitation", json["detail"])
				}
			}()
		}
		wg.Wait()

		assert.Equal(t, uint64(1), succeeded)
	})
}

func TestLogin(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("user can't login with non-existing credentials", func(t *testing.T) {
		response := r.loginUser("test-user@example.com", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
		assert.Equal(t, "Invalid credentials", json["detail"])
	})

	t.Run("user can register with invite key", func(t *testing.T) {
		r.createInvite(t, "test-invite")
		response := r.registerUser("test-user@example.com", "test-user", "test-invite", "test-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/users/1", json["id"])
		assert.Equal(t, "test-user@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"])
	})

	t.Run("user can't login with invalid password", func(t *testing.T) {
		response := r.loginUser("test-user@example.com", "invalid-password")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
		assert.Equal(t, "Invalid credentials", json["detail"])
	})

	var cookies []string
	t.Run("user can login with valid credentials", func(t *testing.T) {
		response := r.loginUser("test-user@example.com", "test-password")
		cookies = response.Cookies
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "Successfully authenticated user", json["detail"])
	})

	t.Run("home page welcomes the authenticated user", func(t *testing.T) {
		response := r.requestHTML("GET", "/", cookies)
		assert.Contains(t, response.BodyString(), `Welcome, <a href="/profile">test-user</a>`)
	})

	t.Run("user can log out", func(t *testing.T) {
		response := r.requestHTML("POST", "/logout", cookies)
		cookies = response.Cookies
		assert.Equal(t, http.StatusFound, response.StatusCode)
		assert.Equal(t, "/", response.Location)
	})

	t.Run("home page no longer welcomes the user", func(t *testing.T) {
		response := r.requestHTML("GET", "/", cookies)
		assert.NotContains(t, response.BodyString(), `Logged in as "test-user@example.com"`)
		assert.Contains(t, response.BodyString(), "see yourself inside")
	})
}

func TestUserEditProfile(t *testing.T) {
	r := NewTestRouterUser(t)
	r.createTestUser(t)
	cookies := r.loginUser("test-user@example.com", "test-password").Cookies

	t.Run("user can GET their own profile", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("GET", "/profile", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})

	t.Run("user can PUT their own profile with new e-mail, password and handle", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("PUT", "/profile", []byte(`{"email":"test-user2@example.com","handle":"test-user2","password":"test-password2"}`), cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "test-user2@example.com", json["email"])
		assert.Equal(t, "test-user2", json["handle"])
	})

	t.Run("user can't PUT their own profile with invalid email", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("PUT", "/profile", []byte(`{"email":"test-user2-at-example.com","handle":"test-user2","password":"test-password2"}`), cookies)
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	})

	t.Run("user can log out", func(t *testing.T) {
		response := r.requestHTML("POST", "/logout", cookies)
		assert.Equal(t, http.StatusFound, response.StatusCode)
		assert.Equal(t, "/", response.Location)
	})

	t.Run("user can login with new e-mail and password", func(t *testing.T) {
		response := r.loginUser("test-user2@example.com", "test-password2")
		cookies = response.Cookies
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "Successfully authenticated user", json["detail"])
	})

	t.Run("home page should show the new handle", func(t *testing.T) {
		response := r.requestHTML("GET", "/", cookies)
		assert.Contains(t, response.BodyString(), `Welcome, <a href="/profile">test-user2</a>`)
	})
}

func TestInviteForwarding(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("GET /register should redirect to /register/", func(t *testing.T) {
		response := r.requestHTML("GET", "/register/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/register", response.Location)
	})

	t.Run("GET /register with invite key should show the registration form with invite key", func(t *testing.T) {
		response := r.requestHTML("GET", "/register?invite=test-invite", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), `value="test-invite"`)
	})
}

func TestUserCreateEntry(t *testing.T) {
	r := NewTestRouterUser(t)
	r.createTestCompo(t)
	r.createCompo(t, "test compo 2", true, true)
	r.createTestUser(t)
	cookies := r.loginUser("test-user@example.com", "test-password").Cookies

	t.Run("user can GET empty list of entries", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("user can't POST entry for compo ID 0", func(t *testing.T) {
		response := r.createUserEntry(0, "test-entry", "test-author", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "CompoID is a required field", json["detail"])
	})

	t.Run("user can POST valid entry", func(t *testing.T) {
		response := r.createUserEntry(1, "entry 1", "author 1", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "author 1", json["author"])
		assert.Equal(t, "/compos/1", json["compo"])
		assert.Equal(t, "/entries/1", json["id"])
		assert.Equal(t, "entry 1", json["title"])
		assert.Equal(t, "**be strong**", json["notes"])
		assert.Equal(t, "/users/1", json["user"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, map[string]interface{}{"id": "/entries/1/preview"}, json["preview"])
	})

	t.Run("user can GET list of entries", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.NotEmpty(t, response.Body)
	})

	t.Run("user can GET their own entry", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("GET", "/entries/1", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "author 1", json["author"])
		assert.Equal(t, "/compos/1", json["compo"])
		assert.Equal(t, "/entries/1", json["id"])
		assert.Equal(t, "entry 1", json["title"])
		assert.Equal(t, "**be strong**", json["notes"])
		assert.Equal(t, "/users/1", json["user"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, map[string]interface{}{"id": "/entries/1/preview"}, json["preview"])
	})

	t.Run("user can't POST entry for non-existent compo", func(t *testing.T) {
		response := r.createUserEntry(2, "entry 2", "author 2", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "compo 2 not found", json["detail"])
	})
}

func TestUserUpdateEntry(t *testing.T) {
	r := NewTestRouterUser(t)
	r.createTestCompo(t)
	r.createCompo(t, "test compo 2", true, true)
	r.createTestUser(t)
	cookies := r.loginUser("test-user@example.com", "test-password").Cookies

	t.Run("user can POST entry", func(t *testing.T) {
		response := r.createUserEntry(1, "entry 1", "author 1", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "author 1", json["author"])
		assert.Equal(t, "/compos/1", json["compo"])
		assert.Equal(t, "/entries/1", json["id"])
		assert.Equal(t, "entry 1", json["title"])
		assert.Equal(t, "**be strong**", json["notes"])
		assert.Equal(t, "/users/1", json["user"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, map[string]interface{}{"id": "/entries/1/preview"}, json["preview"])
	})

	t.Run("user can PUT entry", func(t *testing.T) {
		response := r.updateUserEntry(1, "updated entry 1", "updated author 1", "platform", "notes", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "updated author 1", json["author"])
		assert.Equal(t, "/compos/1", json["compo"])
		assert.Equal(t, "/entries/1", json["id"])
		assert.Equal(t, "updated entry 1", json["title"])
		assert.Equal(t, "notes", json["notes"])
		assert.Equal(t, "/users/1", json["user"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, map[string]interface{}{"id": "/entries/1/preview"}, json["preview"])
	})
}

func TestUserDeleteEntry(t *testing.T) {
	r := NewTestRouterUser(t)

	r.createTestCompo(t)
	r.createTestUser(t)
	cookies := r.loginUser("test-user@example.com", "test-password").Cookies
	r.createUserTestEntry(t, cookies)

	t.Run("user can GET empty entry list", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.NotEmpty(t, response.BodyString())
	})

	t.Run("user can DELETE their own entry", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("DELETE", "/entries/1", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})

	t.Run("user can GET empty entry list", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("GET", "/entries", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("user can't DELETE the same entry twice", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("DELETE", "/entries/1", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestUserEntryPreview(t *testing.T) {
	r := NewTestRouterUser(t)

	r.createTestCompo(t)
	r.createTestUser(t)
	response := r.loginUser("test-user@example.com", "test-password")
	cookies := response.Cookies

	r.createUserTestEntry(t, cookies)

	t.Run("user can GET entry preview", func(t *testing.T) {
		response = r.requestHTML("GET", "/entries/1/preview", cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "test-entry</h1>")
		assert.Contains(t, response.BodyString(), "<h2>By test-author</h2>")
		assert.Contains(t, response.BodyString(), `<p><strong>be strong</strong></p>`)
	})
}

func TestUserCreateUpload(t *testing.T) {
	r := NewTestRouterUser(t)

	r.createTestCompo(t)
	r.createTestUser(t)
	cookies := r.loginUser("test-user@example.com", "test-password").Cookies
	r.createUserTestEntry(t, cookies)

	t.Run("user can POST upload", func(t *testing.T) {
		response := r.uploadFileWithCookies("POST", "/entries/1/uploads", []byte{1, 2, 3}, "file.bin", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "application/octet-stream", json["contentType"])
		assert.Equal(t, "/entries/1", json["entry"])
		assert.Equal(t, "file.bin", json["filename"])
		assert.Equal(t, "/entries/1/uploads/1", json["id"])
	})

	t.Run("user can GET the created upload", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("GET", "/entries/1/uploads/1", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "application/octet-stream", json["contentType"])
		assert.Equal(t, "/entries/1", json["entry"])
		assert.Equal(t, "file.bin", json["filename"])
		assert.Equal(t, "/entries/1/uploads/1", json["id"])
	})

	t.Run("user can GET the data of the created upload", func(t *testing.T) {
		response := r.requestHTML("GET", "/entries/1/uploads/1/data", cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, []byte("\x01\x02\x03"), response.Body)
	})

	t.Run("GET entry with trailing slash should redirect", func(t *testing.T) {
		response := r.requestHTML("GET", "/entries/1/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/entries/1", response.Location)
	})

	t.Run("GET upload with trailing slash should redirect", func(t *testing.T) {
		response := r.requestHTML("GET", "/entries/1/uploads/1/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/entries/1/uploads/1", response.Location)
	})

	t.Run("GET upload data with trailing slash should redirect", func(t *testing.T) {
		response := r.requestHTML("GET", "/entries/1/uploads/1/data/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/entries/1/uploads/1/data", response.Location)
	})

	t.Run("user can't POST upload to a locked entry", func(t *testing.T) {
		t.Run("user can POST entry", func(t *testing.T) {
			response := r.requestJsonApiWithCookies("POST", "/entries", []byte(`{"compo":1, "title":"entry 2", "author":"author 2"}`), cookies)
			json := response.BodyJSON()
			assert.Equal(t, http.StatusCreated, response.StatusCode)
			assert.Equal(t, "/entries/2", json["id"])
			r.lockEntry(2)
		})

		response := r.uploadFileWithCookies("POST", "/entries/2/uploads", []byte{1, 2, 3}, "file.bin", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusMethodNotAllowed, response.StatusCode)
		assert.Equal(t, "Entry has been locked", json["detail"])
	})

	t.Run("user can DELETE their own upload", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("DELETE", "/entries/1/uploads/1", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})
}

func TestUserDeleteUpload(t *testing.T) {
	r := NewTestRouterUser(t)
	r.createTestCompo(t)
	r.createTestUser(t)
	cookies := r.loginUser("test-user@example.com", "test-password").Cookies
	r.createUserTestEntry(t, cookies)
	r.uploadFileWithCookies("POST", "/entries/1/uploads", []byte{1, 2, 3}, "file.bin", cookies)

	t.Run("user can DELETE their own upload", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("DELETE", "/entries/1/uploads/1", nil, cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})

	t.Run("user can't DELETE the same upload twice", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("DELETE", "/entries/1/uploads/1", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestXUserAccess(t *testing.T) {
	r := NewTestRouterUser(t)

	r.createTestCompo(t)
	r.createInvite(t, "test-invite1")
	r.createInvite(t, "test-invite2")
	r.registerUser("test-user1@example.com", "test-user", "test-invite1", "test-password1")
	r.registerUser("test-user2@example.com", "test-user", "test-invite2", "test-password2")
	response := r.loginUser("test-user1@example.com", "test-password1")
	cookies := response.Cookies

	t.Run("user A can POST an upload to their own entry", func(t *testing.T) {
		r.createUserTestEntry(t, cookies)
		r.uploadFileWithCookies("POST", "/entries/1/uploads", []byte{1, 2, 3}, "file.bin", cookies)
	})

	response = r.loginUser("test-user2@example.com", "test-password2")
	cookies = response.Cookies

	t.Run("user B can't GET user A's entry", func(t *testing.T) {
		response = r.requestJsonApiWithCookies("GET", "/entries/1", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("user B can't DELETE user A's entry", func(t *testing.T) {
		response = r.requestJsonApiWithCookies("DELETE", "/entries/1", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("user B can't GET user A's upload", func(t *testing.T) {
		response = r.requestJsonApiWithCookies("GET", "/entries/1/uploads/1", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("user B can't POST an upload to user A's entry", func(t *testing.T) {
		response = r.uploadFileWithCookies("POST", "/entries/1/uploads", []byte{1, 2, 3}, "file.bin", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("user B can't GET the data of user A's upload", func(t *testing.T) {
		response = r.requestJsonApiWithCookies("GET", "/entries/1/uploads/1/data", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("user B can't DELETE user A's upload", func(t *testing.T) {
		response = r.requestJsonApiWithCookies("DELETE", "/entries/1/uploads/1", nil, cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("user B can't PUT to user A's entry", func(t *testing.T) {
		response = r.updateUserEntry(1, "updated entry 1", "updated author 1", "platform", "notes", cookies)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestUserVoting(t *testing.T) {
	r := NewTestRouterUser(t)
	r.createTestCompo(t)
	r.createTestUser(t)
	cookies := r.loginUser("test-user@example.com", "test-password").Cookies

	t.Run("no shown entry should not allow voting", func(t *testing.T) {
		response := r.requestHTML("GET", "/voting", cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "There are currently nothing to live-vote on.")
	})

	playlist := r.createPlaylist(t, "test-playlist", []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10})

	for i := 0; i < 10; i++ {
		r.createUserTestEntry(t, cookies)
	}

	t.Run("show playlist should allow voting for 1st entry", func(t *testing.T) {
		_, err := playlist.Show(r.DB)
		assert.Nil(t, err)
		assert.Equal(t, 10, len(playlist.Entries))

		response := r.requestHTML("GET", "/voting", cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), `<iframe src="/playlists/1/entries/1"></iframe>`)
	})

	t.Run("showing 5 entries should allow voting for the 5th entry", func(t *testing.T) {
		for _, entry := range playlist.Entries {
			if entry.EntryID == 5 {
				err := entry.Show(r.DB)
				assert.NoError(t, err)
				break
			}
		}

		response := r.requestHTML("GET", "/voting", cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), `<iframe src="/playlists/1/entries/5"></iframe>`)
	})

	t.Run("voting for fifth entry should work", func(t *testing.T) {
		response := r.requestJsonApiWithCookies("POST", "/playlists/1/entries/5/votes", []byte(`{"score":3}`), cookies)
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, 3.0, response.BodyJSON()["score"])
	})

	t.Run("fifth entry should have a score of 3", func(t *testing.T) {
		response := r.requestHTML("GET", "/voting", cookies)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), `<li class="star star-3 selected">`)
	})
}

func Test404Error(t *testing.T) {
	r := NewTestRouterUser(t)

	t.Run("non-existent route should invoke custom error handling", func(t *testing.T) {
		response := r.requestHTML("GET", "/non-exitent-route", nil)
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Contains(t, response.BodyString(), "<h2>ERROR 404</h2>")
		assert.Contains(t, response.BodyString(), "File not found!")
	})
}
