package main

import (
	"bytes"
	"fmt"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestRoutes(t *testing.T) {
	r := NewTestRouterAdmin(t)

	response := r.fetchAdminAPIEndpoint("GET", "/", nil)
	assert.Equal(t, http.StatusOK, response.StatusCode)
}

func TestInvites(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /invites returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/invites", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("creating an invite works", func(t *testing.T) {
		response := r.adminCreateInvite("test-invite")
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "test-invite", response.BodyJSON()["key"])
	})

	t.Run("GET /invites returns the created invite", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/invites", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{"key":"test-invite"}]`, response.BodyString())
	})
}

func TestComposEmpty(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /compos/ redirects to /compos", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/compos", response.Location)
	})

	t.Run("GET /compos returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("GET /compos/1 returns 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("GET /compos/18446744073709551615 returns 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos/18446744073709551615", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("GET /compos/340282366920938463463374607431768211455 returns 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos/340282366920938463463374607431768211455", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestCreateCompo(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /compos returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("POST /compos creates a compo", func(t *testing.T) {
		response := r.adminCreateCompo("test compo", true, false)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/compos/1", json["id"])
		assert.Equal(t, "test compo", json["name"])
		assert.Equal(t, true, json["multiPlatform"])
		assert.Equal(t, false, json["locked"])
	})

	t.Run("GET /compos returns created compo", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"/compos/1",
			"name":"test compo",
			"multiPlatform":true,
			"locked":false
		}]`, response.BodyString())
	})

	t.Run("GET /compos/1/ redirects to /compos/1", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos/1/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/compos/1", response.Location)
	})

	t.Run("GET /compos/1 returns created compo", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos/1", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "/compos/1", json["id"])
		assert.Equal(t, "test compo", json["name"])
		assert.Equal(t, true, json["multiPlatform"])
		assert.Equal(t, false, json["locked"])
	})

	t.Run("DELETE /compos/1 works", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("DELETE", "/compos/1", nil)
		// TODO: This should preferably return 204 No Content
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "{}", response.BodyString())
	})

	t.Run("GET /compos returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})
}

func TestUpdateCompo(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("create test compo", func(t *testing.T) {
		r.createTestCompo(t)
	})

	t.Run("GET /compos returns created test compo", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"/compos/1",
			"name":"test compo",
			"multiPlatform":true,
			"locked":false
		}]`, response.BodyString())
	})

	t.Run("updating compo works", func(t *testing.T) {
		response := r.updateCompo(1, gin.H{
			"name":          "updated test compo",
			"multiPlatform": false,
			"locked":        true,
		})
		assert.Equal(t, http.StatusOK, response.StatusCode)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "updated test compo", json["name"])
		assert.Equal(t, false, json["multiPlatform"])
		assert.Equal(t, true, json["locked"])
	})

	t.Run("GET /compos returns updated compo", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"/compos/1",
			"name":"updated test compo",
			"multiPlatform":false,
			"locked":true
		}]`, response.BodyString())
	})
}

func TestCompoArchive(t *testing.T) {
	r := NewTestRouterAdmin(t)
	r.createTestCompo(t)
	r.createEntry(1, "test-entry", "test-author")
	r.createUpload(1, "test.txt", []byte("test-upload"))

	t.Run("GET compo archive returns expected ZIP file", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos/1/archive", nil)
		expectedZip := []byte("PK\x03\x04\x14\x00\b\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\b\x00\x00\x00test.txt*I-.\xd1--\xc8\xc9OL\x01\x04\x00\x00\xff\xffPK\a\b3 \x93\xfb\x11\x00\x00\x00\v\x00\x00\x00PK\x01\x02\x14\x00\x14\x00\b\x00\b\x00\x00\x00\x00\x003 \x93\xfb\x11\x00\x00\x00\v\x00\x00\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00test.txtPK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x006\x00\x00\x00G\x00\x00\x00\x00\x00")
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, expectedZip, response.Body)
	})
}

func TestCreateEntry(t *testing.T) {
	r := NewTestRouterAdmin(t)

	r.createTestCompo(t)

	t.Run("GET /entries returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("POST /entries creates an entry", func(t *testing.T) {
		response := r.createEntry(1, "test-entry", "test-author")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/entries/1", json["id"])
		assert.Equal(t, "/compos/1", json["compo"])
		assert.Equal(t, "test-author", json["author"])
		assert.Equal(t, "test-entry", json["title"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, "/entries/1/preview", json["preview"].(map[string]interface{})["id"])
	})

	t.Run("GET /entries returns array with created entry", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"/entries/1",
			"compo":"/compos/1",
			"author":"test-author",
			"title":"test-entry",
			"locked":false,
			"preview": { "id": "/entries/1/preview" }
		}]`, response.BodyString())
	})

	t.Run("GET /entries/1 returns created entry", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "/entries/1", json["id"])
		assert.Equal(t, "/compos/1", json["compo"])
		assert.Equal(t, "test-author", json["author"])
		assert.Equal(t, "test-entry", json["title"])
		assert.Equal(t, false, json["locked"])
		assert.Equal(t, "/entries/1/preview", json["preview"].(map[string]interface{})["id"])
	})

	t.Run("GET /entries/2 returns 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/2", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("creating entry for non-existent compo fails with 404", func(t *testing.T) {
		response := r.createEntry(2, "test-entry", "test-author")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestUpdateEntry(t *testing.T) {
	r := NewTestRouterAdmin(t)

	r.createTestCompo(t)

	t.Run("create entry", func(t *testing.T) {
		response := r.createEntry(1, "test-entry", "test-author")
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/entries/1", json["id"])
	})

	t.Run("updating non-existing entry fails with 404", func(t *testing.T) {
		response := r.updateEntry(2, gin.H{
			"compo":  1,
			"author": "test-author",
			"title":  "updated test-entry",
			"user":   0,
			"locked": false,
		})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("updating existing entry works", func(t *testing.T) {
		response := r.updateEntry(1, gin.H{
			"compo":  1,
			"author": "updated test-author",
			"title":  "updated test-entry",
			"user":   0,
			"locked": true,
		})
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "/entries/1", json["id"])
		assert.Equal(t, "/compos/1", json["compo"])
		assert.Equal(t, "updated test-author", json["author"])
		assert.Equal(t, "updated test-entry", json["title"])
		assert.Equal(t, true, json["locked"])
		assert.Equal(t, "/entries/1/preview", json["preview"].(map[string]interface{})["id"])
	})
}

func TestDeleteEntry(t *testing.T) {
	r := NewTestRouterAdmin(t)

	r.createTestCompo(t)

	t.Run("create entry", func(t *testing.T) {
		response := r.createEntry(1, "test-entry", "test-author")
		assert.Equal(t, http.StatusCreated, response.StatusCode)
	})

	t.Run("deleting non-existing entry fails with 404", func(t *testing.T) {
		response := r.deleteEntry(2)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("deleting existing entry works", func(t *testing.T) {
		response := r.deleteEntry(1)
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
	})
}

func TestAdminEntryPreview(t *testing.T) {
	r := NewTestRouterAdmin(t)
	r.createTestCompo(t)
	r.createEntry(1, "test-entry", "test-author")

	t.Run("GET /entries/1/preview returns expected HTML", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/1/preview", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "test-entry</h1>")
		assert.Contains(t, response.BodyString(), "<h2>By test-author</h2>")
	})
}

func TestCreateUpload(t *testing.T) {
	r := NewTestRouterAdmin(t)

	r.createTestCompo(t)
	r.createTestEntry(t)

	t.Run("GET /entries/1/uploads returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/1/uploads", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("creating upload for non-existing entry fails with 404", func(t *testing.T) {
		response := r.createUpload(2, "test.txt", []byte("test-upload"))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("creating upload for existing entry works", func(t *testing.T) {
		response := r.createUpload(1, "test.txt", []byte("test-upload"))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/entries/1/uploads/1", json["id"])
		assert.Equal(t, "/entries/1", json["entry"])
		assert.Equal(t, "test.txt", json["filename"])
		assert.Equal(t, "text/plain; charset=utf-8", json["contentType"])
	})

	t.Run("GET /entries/1/uploads returns array with created upload", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/1/uploads", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"contentType":"text/plain; charset=utf-8",
			"entry":"/entries/1",
			"filename":"test.txt",
			"id":"/entries/1/uploads/1"
		}]`, response.BodyString())
	})

	t.Run("GET /entries/1/uploads/1 returns created upload", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/1/uploads/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "/entries/1/uploads/1", json["id"])
		assert.Equal(t, "/entries/1", json["entry"])
		assert.Equal(t, "test.txt", json["filename"])
		assert.Equal(t, "text/plain; charset=utf-8", json["contentType"])
	})

	t.Run("GET /entries/1/uploads/1/data returns uploaded data", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/1/uploads/1/data", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "test-upload", response.BodyString())
	})

	t.Run("GET /entries/0/uploads/1 returns 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/0/uploads/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("GET /entries/0/uploads/1/data returns 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/0/uploads/1/data", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestDeleteUpload(t *testing.T) {
	r := NewTestRouterAdmin(t)

	r.createTestCompo(t)
	r.createTestEntry(t)

	t.Run("create upload", func(t *testing.T) {
		response := r.createUpload(1, "test.txt", []byte("test-upload"))
		assert.Equal(t, http.StatusCreated, response.StatusCode)
	})

	t.Run("DELETE /entries/0/uploads/1 fails with 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("DELETE", "/entries/0/uploads/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("DELETE created upload succeeds", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("DELETE", "/entries/1/uploads/1", nil)
		// TODO: This should preferably return 204 No Content
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "{}", response.BodyString())
	})

	t.Run("GET /entries/1/uploads returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/1/uploads", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("DELETE already deleted uploads fails with 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("DELETE", "/entries/1/uploads/1", nil)
		json := response.BodyJSON()
		// TODO: This should preferably return `410 Gone`, since the resource once did exist.
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})
}

func TestUserAdministration(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /users/ redirects to /users", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/users/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/users", response.Location)
	})

	t.Run("GET /users returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/users", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("POST /users creates user", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/users", bytes.NewBuffer([]byte(`{"email":"user1@example.com", "handle":"test-user"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/users/1", json["id"])
		assert.Equal(t, "user1@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"])
	})

	t.Run("GET /users returns created user", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/users", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"id":"/users/1",
			"email":"user1@example.com",
			"handle":"test-user"
		}]`, response.BodyString())
	})

	t.Run("GET /users/1/ redirects to /users/1", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/users/1/", nil)
		assert.Equal(t, http.StatusMovedPermanently, response.StatusCode)
		assert.Equal(t, "/users/1", response.Location)
	})

	t.Run("GET /users/1 returns created user", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/users/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "/users/1", json["id"])
		assert.Equal(t, "user1@example.com", json["email"])
		assert.Equal(t, "test-user", json["handle"])
	})

	t.Run("PUT /users/1 updates user", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PUT", "/users/1", bytes.NewBuffer([]byte(`{"email":"user2@example.com", "handle":"test-user2"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "/users/1", json["id"])
		assert.Equal(t, "user2@example.com", json["email"])
		assert.Equal(t, "test-user2", json["handle"])
	})

	t.Run("DELETE /users/1 succeeds", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("DELETE", "/users/1", bytes.NewBuffer([]byte(`{"email":"user2@example.com", "handle":"test-user2"}`)))
		assert.Equal(t, http.StatusNoContent, response.StatusCode)
	})
}

func TestPlaylists(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("GET /playlists returns empty array", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/playlists", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "[]", response.BodyString())
	})

	t.Run("GET non-existing playlist returns 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/playlists/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	t.Run("POST /playlists creates playlist", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/playlists", bytes.NewBuffer([]byte(`{"name":"Playlist 1"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/playlists/1", json["id"])
		assert.Equal(t, "Playlist 1", json["name"])
	})

	t.Run("GET /playlists returns array with created playlist", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/playlists/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "/playlists/1", json["id"])
		assert.Equal(t, "Playlist 1", json["name"])
	})

	t.Run("POST playlist entry with reference to non-existing entry fails with 404", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/playlists/1/entries", bytes.NewBuffer([]byte(`{"entry":1}`)))
		json := response.BodyJSON()
		// TODO: It's not actually correct to fail with 404 for references that don't exist.
		//       404 only applies to the resource identified by the URI. As long as the URI
		//       resolves, 404 is not the correct response. We should respond with 400 or 422
		//       here (and everywhere else with similar error handling) instead.
		assert.Equal(t, http.StatusNotFound, response.StatusCode)
		assert.Equal(t, "File not found!", json["detail"])
	})

	r.createTestCompo(t)
	r.createEntry(1, "test-entry", "test-author")

	t.Run("POST playlist entry with reference to existing entry creates playlist entry", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/playlists/1/entries", bytes.NewBuffer([]byte(`{"entry":1}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/playlists/1/entries/1", json["id"])
		assert.Equal(t, float64(1), json["order"])
		assert.Equal(t, "/playlists/1", json["playlist"].(map[string]interface{})["id"])
		assert.Equal(t, "/playlists/1/entries/1/votes", json["votes"].(map[string]interface{})["id"])
		assert.Equal(t, map[string]any{
			"author":  "test-author",
			"id":      "/entries/1",
			"locked":  false,
			"title":   "test-entry",
			"preview": map[string]any{"id": "/entries/1/preview"},
		}, json["entry"])
	})

	t.Run("DELETE created playlist entry succeeds", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("DELETE", "/playlists/1/entries/1", nil)
		// TODO: This should preferably return 204 No Content
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})

	t.Run("DELETE created playlist succeeds", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("DELETE", "/playlists/1", nil)
		// TODO: This should preferably return 204 No Content
		assert.Equal(t, http.StatusOK, response.StatusCode)
	})

	t.Run("POST /playlists creates playlist", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/playlists", bytes.NewBuffer([]byte(`{"name": "Playlist 2"}}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, "/playlists/2", json["id"])
		assert.Equal(t, "Playlist 2", json["name"])
	})

	t.Run(`PATCH /playlists/2 with "action=show" shows playlist`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists/2", bytes.NewBuffer([]byte(`{"action":"show"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["shownAt"])
	})

	t.Run("POST /playlists creates playlist", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/playlists", bytes.NewBuffer([]byte(`{"name": "Playlist 3", "entries":[{"entry":1}]}`)))
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.JSONEq(t, `{
			"id":"/playlists/3",
			"name":"Playlist 3",
			"entries":"/playlists/3/entries/"
		}`, response.BodyString())
	})

	t.Run("GET /playlists/3/entries returns array with created playlist entry", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/playlists/3/entries", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.JSONEq(t, `[{
			"entry":{
				"author":"test-author",
				"id":"/entries/1",
				"locked":false,
				"title":"test-entry",
				"preview": { "id": "/entries/1/preview" }
			},
			"id":"/playlists/3/entries/1",
			"order":0,
			"playlist":{"id":"/playlists/3", "name":"Playlist 3"},
			"votes": {"id":"/playlists/3/entries/1/votes"}
		}]`, response.BodyString())
	})

	t.Run("create entry", func(t *testing.T) {
		r.createEntry(1, "test-entry", "test-author")
	})

	t.Run("POST playlist entry succeeds", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/playlists/3/entries", bytes.NewBuffer([]byte(`{"entry":2}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, gin.H{
			"entry": map[string]any{
				"author":  "test-author",
				"id":      "/entries/2",
				"locked":  false,
				"title":   "test-entry",
				"preview": map[string]any{"id": "/entries/2/preview"},
			},
			"id":       "/playlists/3/entries/2",
			"order":    float64(2),
			"playlist": map[string]any{"id": "/playlists/3", "name": "Playlist 3"},
			"votes":    map[string]any{"id": "/playlists/3/entries/2/votes"},
		}, json)
	})

	t.Run(`PATCH playlist entry with "direction=down" increments "order"`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists/3/entries/1", bytes.NewBuffer([]byte(`{"direction":"down"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, float64(2), json["order"])
	})

	t.Run("GET playlist preview succeeds", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/playlists/3/preview", nil)
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, "/playlists/3/preview", json["id"])
		assert.Equal(t, "Playlist 3", json["name"])
		assert.Equal(t, "/playlists/3/entries/", json["entries"])
	})

	t.Run("create entry", func(t *testing.T) {
		r.createEntry(1, "test-entry2", "test-author2")
	})

	t.Run("POST playlist entry succeeds", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/playlists/3/entries", bytes.NewBuffer([]byte(`{"entry":3}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusCreated, response.StatusCode)
		assert.Equal(t, gin.H{
			"entry": map[string]any{
				"author":  "test-author2",
				"id":      "/entries/3",
				"locked":  false,
				"title":   "test-entry2",
				"preview": map[string]any{"id": "/entries/3/preview"},
			},
			"id":    "/playlists/3/entries/3",
			"order": float64(3),
			"playlist": map[string]any{
				"id":   "/playlists/3",
				"name": "Playlist 3",
			},
			"votes": map[string]any{
				"id": "/playlists/3/entries/3/votes",
			},
		}, json)
	})

	t.Run("GET playlist preview with out of bounds slide numbers succeeds", func(t *testing.T) {
		for i := 0; i < 4; i++ {
			response := r.fetchAdminAPIEndpoint("GET", fmt.Sprintf("/playlists/3/preview/%d", i), nil)
			json := response.BodyJSON()
			assert.Equal(t, http.StatusOK, response.StatusCode)
			assert.Equal(t, "/playlists/3/preview", json["id"])
			assert.Equal(t, "Playlist 3", json["name"])
			assert.Equal(t, "/playlists/3/entries/", json["entries"])
		}
	})

	t.Run(`PATCH playlist entry with "action=invalid" fails with 400`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists/3", bytes.NewBuffer([]byte(`{"action":"invalid"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusBadRequest, response.StatusCode)
		assert.Equal(t, "invalid action: invalid", json["detail"])
	})

	t.Run(`PATCH playlist with "action=show" shows playlist`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists/3", bytes.NewBuffer([]byte(`{"action":"show"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["shownAt"])
	})

	t.Run("compo should be locked after having one or more of its entries shown in a playlist", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/compos/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, true, json["locked"])
	})

	t.Run("entry 1 should be locked after being shown in a playlist", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/1", nil)
		json := response.BodyJSON()
		assert.Equal(t, true, json["locked"])
	})

	t.Run("entry 2 should be locked after being shown in a playlist", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/entries/2", nil)
		json := response.BodyJSON()
		assert.Equal(t, true, json["locked"])
	})

	t.Run(`POST "action=show" to playlist slide 2 updates shownAt`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("POST", "/playlists/3/show/2", bytes.NewBuffer([]byte(`{"action":"show"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["shownAt"])
	})

	t.Run(`PATCH playlist with "action=close" closes live voting, but not regular voting`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists/3", bytes.NewBuffer([]byte(`{"action":"close"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["liveVotingClosedAt"])
		assert.Nil(t, json["votingClosedAt"])
	})

	t.Run(`PATCH playlist with "action=close" again closes regular voting`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists/3", bytes.NewBuffer([]byte(`{"action":"close"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["liveVotingClosedAt"])
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["votingClosedAt"])
	})

	t.Run(`PATCH all playlists with "action=close" closes voting for all playlists`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists", bytes.NewBuffer([]byte(`{"action":"close"}`)))
		assert.Equal(t, http.StatusOK, response.StatusCode)

		for i := 2; i <= 3; i++ {
			path := fmt.Sprintf("/playlists/%d", i)
			t.Run(fmt.Sprintf("GET %s confirms voting is closed", path), func(t *testing.T) {
				response := r.fetchAdminAPIEndpoint("GET", path, nil)
				json := response.BodyJSON()
				assert.Equal(t, http.StatusOK, response.StatusCode)
				assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["liveVotingClosedAt"])
				assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["votingClosedAt"])
			})
		}
	})

	t.Run(`PATCH playlist with "action=open" opens regular voting, but not live voting`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists/3", bytes.NewBuffer([]byte(`{"action":"open"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Regexp(t, `\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.*`, json["liveVotingClosedAt"])
		assert.Nil(t, json["votingClosedAt"])
	})

	t.Run(`PATCH playlist with "action=open" again opens live voting`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("PATCH", "/playlists/3", bytes.NewBuffer([]byte(`{"action":"open"}`)))
		json := response.BodyJSON()
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Nil(t, json["liveVotingClosedAt"])
		assert.Nil(t, json["votingClosedAt"])
	})
}

func TestPlaylistArchive(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("create compo, entry, upload and playlist", func(t *testing.T) {
		r.createTestCompo(t)
		r.createEntry(1, "test-entry", "test-author")
		r.createUpload(1, "test.txt", []byte("test-upload"))
		r.fetchAdminAPIEndpoint("POST", "/playlists", bytes.NewBuffer([]byte(`{"name": "Playlist 1", "entries":[{"entry":1}]}`)))
	})

	t.Run("GET playlist archive returns expected ZIP file", func(t *testing.T) {
		expectedZip := []byte("PK\x03\x04\x14\x00\b\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\b\x00\x00\x00test.txt*I-.\xd1--\xc8\xc9OL\x01\x04\x00\x00\xff\xffPK\a\b3 \x93\xfb\x11\x00\x00\x00\v\x00\x00\x00PK\x01\x02\x14\x00\x14\x00\b\x00\b\x00\x00\x00\x00\x003 \x93\xfb\x11\x00\x00\x00\v\x00\x00\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00test.txtPK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x006\x00\x00\x00G\x00\x00\x00\x00\x00")
		response := r.fetchAdminAPIEndpoint("GET", "/playlists/1/archive", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, expectedZip, response.Body)
	})

	t.Run(`GET playlist archive with "ordered=true" returns expected ZIP file`, func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/playlists/1/archive?ordered=true", nil)
		expectedZip := []byte("PK\x03\x04\x14\x00\b\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1d\x00\x00\x0000_test-author-test-entry.zip\x01\x00\x00\xff\xffPK\a\b\x00\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00PK\x01\x02\x14\x00\x14\x00\b\x00\b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x1d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0000_test-author-test-entry.zipPK\x05\x06\x00\x00\x00\x00\x01\x00\x01\x00K\x00\x00\x00P\x00\x00\x00\x00\x00")
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Equal(t, expectedZip, response.Body)
	})
}

func TestPlaylistEntryPreview(t *testing.T) {
	r := NewTestRouterAdmin(t)

	t.Run("create compo, entry and playlist", func(t *testing.T) {
		r.createTestCompo(t)
		r.createEntry(1, "test-entry", "test-author")
		r.fetchAdminAPIEndpoint("POST", "/playlists", bytes.NewBuffer([]byte(`{"name": "Playlist 1", "entries":[{"entry":1}]}`)))
	})

	t.Run("GET playlist entry preview returns expected HTML", func(t *testing.T) {
		response := r.fetchAdminAPIEndpoint("GET", "/playlists/1/entries/1", nil)
		assert.Equal(t, http.StatusOK, response.StatusCode)
		assert.Contains(t, response.BodyString(), "test-entry</h1>")
	})
}
