package main

import (
	"io"
	"log"
	"os"
	"strings"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

type Config struct {
	Secret       []byte
	DatabasePath string
	Admins       map[string]string
	Logging      LogConfig
	Routing      struct {
		MinimumPasswordLength int
	}
	RateLimit struct {
		Register uint32
		Login    uint32
	}
}

type LogLevel uint

const (
	Silent LogLevel = iota + 1
	Error
	Warn
	Info
)

type LogConfig struct {
	Writer io.Writer
	Level  LogLevel
}

func (logLevel *LogLevel) UnmarshalText(text []byte) error {
	logLevelMap := map[string]LogLevel{
		"silent": Silent,
		"error":  Error,
		"warn":   Warn,
		"info":   Info,
	}
	key := strings.ToLower(string(text))
	if l, ok := logLevelMap[key]; ok {
		*logLevel = l
	}

	return nil
}

func main() {
	viper.SetDefault("routing.MinimumPasswordLength", 5)
	viper.SetDefault("logging.Level", Info)
	viper.SetDefault("DatabasePath", "db/database.db")
	viper.SetDefault("rateLimit.register", 1)
	viper.SetDefault("rateLimit.login", 10)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal(err)
	}

	config := Config{Secret: secret()}

	viper.Unmarshal(&config, viper.DecodeHook(mapstructure.TextUnmarshallerHookFunc()))

	if len(os.Args) < 2 {
		log.Fatal("Please specify a subcommand.")
	}

	cmd := os.Args[1]
	switch cmd {
	case "server":
		userServer(config)
	case "server-admin":
		adminServer(config)
	default:
		log.Fatalf("Unrecognized command %q. "+
			"Command must be one of: server, server-admin", cmd)
	}
}
