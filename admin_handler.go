package main

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gosimple/slug"
	"github.com/spf13/cast"
	"gorm.io/gorm"

	. "granola/models"
)

type AdminHandler struct {
	BaseHandler
}

func (h *AdminHandler) showAdmin(c *gin.Context) {
	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Admin",
	}
	c.HTML(http.StatusOK, "admin/admin", data)
}

func (h *AdminHandler) getInvites(c *gin.Context) {
	invites := []Invite{}
	result := h.DB.Preload("User").Find(&invites)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":  &h.Config.Routing,
		"Title":   "Invites",
		"Invites": invites,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/invites",
		HTMLData: &data,
		Data:     &invites,
	})
}

func (h *AdminHandler) postInvite(c *gin.Context) {
	args := struct {
		Key string `json:"key" form:"key" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	invite, err := NewInvite(h.DB, args.Key)
	if err != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, err.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/invites")
	} else {
		c.JSON(http.StatusCreated, &invite)
	}
}

func (h *AdminHandler) getUsers(c *gin.Context) {
	users := []User{}
	result := h.DB.Find(&users)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Users",
		"Users":  users,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/users",
		HTMLData: &data,
		Data:     users,
	})
}

func (h *AdminHandler) postUser(c *gin.Context) {
	args := struct {
		Email  string `json:"email" form:"email" binding:"required"`
		Handle string `json:"handle" form:"handle" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user := User{
		Email:  args.Email,
		Handle: args.Handle,
	}
	result := h.DB.Create(&user)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/users")
	} else {
		c.JSON(http.StatusCreated, &user)
	}
}

func (h *AdminHandler) getUser(c *gin.Context) {
	user := User{}
	result := h.DB.First(&user, c.Param("user_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "User",
		"User":   &user,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/user",
		HTMLData: &data,
		Data:     &user,
	})
}

func (h *AdminHandler) putUser(c *gin.Context) {
	args := struct {
		Email  string `json:"email" form:"email" binding:"required"`
		Handle string `json:"handle" form:"handle" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user := User{}
	result := h.DB.First(&user, c.Param("user_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	user.Email = args.Email
	user.Handle = args.Handle

	result = h.DB.Save(&user)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, user.Path())
	} else {
		c.JSON(http.StatusOK, &user)
	}
}

func (h *AdminHandler) deleteUser(c *gin.Context) {
	result := h.DB.Delete(&User{}, c.Param("user_id"))
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	c.Status(http.StatusNoContent)
}

func (h *AdminHandler) getCompos(c *gin.Context) {
	compos := []Compo{}
	result := h.DB.Preload("Entries").Find(&compos)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Compos",
		"Compos": compos,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/compos",
		HTMLData: &data,
		Data:     compos,
	})
}

func (h *AdminHandler) postCompo(c *gin.Context) {
	args := struct {
		Name          string `json:"name" form:"name" binding:"required"`
		MultiPlatform *bool  `json:"multiPlatform" form:"multiplatform" binding:"required"`
		Locked        *bool  `json:"locked" form:"locked"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	locked := false
	if args.Locked != nil {
		locked = *args.Locked
	}

	compo, err := NewCompo(h.DB, args.Name, *args.MultiPlatform, locked)
	if err != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, err.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/compos")
	} else {
		c.JSON(http.StatusCreated, &compo)
	}
}

func (h *AdminHandler) getCompo(c *gin.Context) {
	compo := Compo{}
	result := h.DB.Preload("Entries.Uploads").First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Compo",
		"Compo":  &compo,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/compo",
		HTMLData: &data,
		Data:     &compo,
	})
}

func (h *AdminHandler) putCompo(c *gin.Context) {
	args := struct {
		Name          string `json:"name" form:"name" binding:"required"`
		MultiPlatform *bool  `json:"multiPlatform" form:"multiplatform" binding:"required"`
		Locked        *bool  `json:"locked" form:"locked" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := Compo{}
	result := h.DB.First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	compo.Name = args.Name
	compo.MultiPlatform = *args.MultiPlatform
	compo.Locked = *args.Locked

	result = h.DB.Save(&compo)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, compo.Path())
	} else {
		c.JSON(http.StatusOK, &compo)
	}
}

func (h *AdminHandler) deleteCompo(c *gin.Context) {
	result := h.DB.Delete(&Compo{}, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/compos")
	} else {
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (h *AdminHandler) getEntries(c *gin.Context) {
	entries := []Entry{}
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		Preload("Compo").
		Find(&entries)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	data := gin.H{
		"Config":  &h.Config.Routing,
		"Title":   "Entries",
		"Entries": entries,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/entries",
		HTMLData: &data,
		Data:     entries,
	})
}

func (h *AdminHandler) postCompoEntry(c *gin.Context) {
	args := struct {
		Title  string `json:"title" form:"title" binding:"required"`
		Author string `json:"author" form:"author" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := Compo{}
	result := h.DB.First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	entry := Entry{
		Title:  args.Title,
		Author: args.Author,
		Compo:  compo,
	}

	result = h.DB.Create(&entry)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, compo.Path())
	} else {
		c.JSON(http.StatusCreated, &entry)
	}
}

func (h *AdminHandler) getEntry(c *gin.Context) {
	entry := Entry{}
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		Preload("Uploads.Entry").
		Preload("Compo").
		First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	compos := []Compo{}
	result = h.DB.Find(&compos)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Entry",
		"Entry":  &entry,
		"Compos": compos,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/entry",
		HTMLData: &data,
		Data:     &entry,
	})
}

func (h *AdminHandler) getEntryPreview(c *gin.Context) {
	entry := Entry{}
	result := h.DB.
		Preload("Compo").
		Preload("User").
		Preload("Uploads.Entry").
		First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	c.HTML(http.StatusOK, "entry-preview", gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Entry",
		"Entry":       &entry,
		"EntryNumber": 0,
	})
}

func (h *AdminHandler) getPlaylistEntry(c *gin.Context) {
	playlistEntry := PlaylistEntry{}
	result := h.DB.
		Preload("Playlist").
		Preload("Entry.Uploads").
		Where("playlist_id = ? and entry_id = ?",
			c.Param("playlist_id"),
			c.Param("entry_id")).
		First(&playlistEntry)
	if result.Error != nil {
		panic(result.Error)
	}

	c.HTML(http.StatusOK, "entry-preview", gin.H{
		"Config":      &h.Config.Routing,
		"Title":       "Preview Entry",
		"Entry":       &playlistEntry.Entry,
		"EntryNumber": playlistEntry.Order,
		"Playlist":    &playlistEntry.Playlist,
	})
}

func (h *AdminHandler) putEntry(c *gin.Context) {
	args := struct {
		Title    string `json:"title" form:"title" binding:"required"`
		Author   string `json:"author" form:"author" binding:"required"`
		Platform string `json:"platform" form:"platform"`
		Notes    string `json:"notes" form:"notes"`
		CompoID  uint   `json:"compo" form:"compo" binding:"required"`
		Locked   *bool  `json:"locked" form:"locked" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := Compo{}
	result := h.DB.First(&compo, args.CompoID)
	if result.Error != nil {
		panic(fmt.Errorf("compo %d not found", args.CompoID))
	}

	entry := Entry{}
	result = h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	entry.Title = args.Title
	entry.Author = args.Author
	entry.Platform = args.Platform
	entry.Notes = args.Notes
	entry.Compo = compo
	entry.Locked = *args.Locked

	result = h.DB.Save(&entry)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.Path())
	} else {
		c.JSON(http.StatusOK, &entry)
	}
}

func (h *AdminHandler) deleteEntry(c *gin.Context) {
	result := h.DB.Delete(&Entry{}, c.Param("entry_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h *AdminHandler) getUploads(c *gin.Context) {
	uploads := []Upload{}
	result := h.DB.Omit("Content").Preload("Entry").Find(&uploads)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	data := gin.H{
		"Config":  &h.Config.Routing,
		"Title":   "Uploads",
		"Uploads": uploads,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/uploads",
		HTMLData: &data,
		Data:     uploads,
	})
}

func (h *AdminHandler) postUpload(c *gin.Context) {
	formFile, err := c.FormFile("file")
	if err != nil {
		panic(err)
	}
	file, err := formFile.Open()
	if err != nil {
		panic(err)
	}

	content, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	entry := Entry{}
	result := h.DB.
		Preload("Uploads", func(tx *gorm.DB) *gorm.DB { return tx.Omit("Content") }).
		First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	upload, err := NewUpload(h.DB, formFile.Filename, content, entry)
	if err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, upload.Path())
	} else {
		c.JSON(http.StatusCreated, &upload)
	}
}

func (h *AdminHandler) getUpload(c *gin.Context) {
	upload := Upload{}
	result := h.DB.Omit("Content").
		Preload("Entry").
		Preload("Entry.Compo").
		Where("entry_id = ?", c.Param("entry_id")).
		First(&upload, c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Upload",
		"Upload": &upload,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/upload",
		HTMLData: &data,
		Data:     &upload,
	})
}

func (h *AdminHandler) getUploadData(c *gin.Context) {
	upload := Upload{}
	result := h.DB.Where("entry_id = ?", c.Param("entry_id")).First(&upload, c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}
	c.Header("Content-Disposition", "attachment; filename="+upload.Filename)
	c.Data(http.StatusOK, "application/octet-stream", upload.Content)
}

func (h *AdminHandler) deleteUpload(c *gin.Context) {
	result := h.DB.Where("entry_id = ?", c.Param("entry_id")).Delete(&Upload{}, c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (h *AdminHandler) getPlaylists(c *gin.Context) {
	playlists := []Playlist{}
	result := h.DB.Preload("Entries").Find(&playlists)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":    &h.Config.Routing,
		"Title":     "Playlists",
		"Playlists": playlists,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/playlists",
		HTMLData: &data,
		Data:     playlists,
	})
}

func (h *AdminHandler) putPlaylist(c *gin.Context) {
	args := struct {
		Name string `json:"name" form:"name" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	playlist := Playlist{}
	result := h.DB.First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	playlist.Name = args.Name
	result = h.DB.Save(&playlist)
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusOK, &playlist)
	}
}

func (h *AdminHandler) postPlaylist(c *gin.Context) {
	args := struct {
		Name    string `json:"name" form:"name" binding:"required"`
		Entries []struct {
			EntryID uint `json:"entry" form:"entry" binding:"required"`
		} `json:"entries" form:"entries"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	playlist := Playlist{Name: args.Name, Entries: []PlaylistEntry{}}
	tx := h.DB.Begin()
	if err := tx.Create(&playlist).Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	// This feels very silly, but I can't get Gorm to both save and set this association automatically.
	for _, e := range args.Entries {
		entry, err := h.mapPlaylistEntry(tx, e.EntryID, playlist)
		if err != nil {
			tx.Rollback()
			panic(err)
		}
		playlist.Entries = append(playlist.Entries, *entry)
	}

	if err := tx.CreateInBatches(&playlist.Entries, 100).Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Commit().Error; err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusCreated, &playlist)
	}
}

func (h *AdminHandler) patchPlaylists(c *gin.Context) {
	type PlaylistAction string
	const (
		Close PlaylistAction = "close"
	)
	args := struct {
		Action PlaylistAction `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	playlists := []Playlist{}
	now := time.Now()
	tx := h.DB.Begin()
	if err := tx.
		Model(&playlists).
		Where("voting_closed_at is null").
		UpdateColumn("voting_closed_at", now).
		Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.
		Model(&playlists).
		Where("live_voting_closed_at is null").
		UpdateColumn("live_voting_closed_at", now).
		Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Find(&playlists).Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Commit().Error; err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/playlists")
	} else {
		c.JSON(http.StatusOK, &playlists)
	}
}

func (h *AdminHandler) postPlaylistEntry(c *gin.Context) {
	args := struct {
		EntryID uint `json:"entry" form:"entry" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	playlist := Playlist{}
	playlistId := c.Param("playlist_id")
	tx := h.DB.Begin()
	if err := tx.First(&playlist, playlistId).Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	playlistEntry, err := h.mapPlaylistEntry(tx, args.EntryID, playlist)
	if err != nil {
		tx.Rollback()
		panic(err)
	}

	playlistEntryCount := uint(0)
	if err := tx.
		Table("playlist_entries").
		Where("playlist_id = ?", playlistId).
		Select("count(entry_id)").
		Row().
		Scan(&playlistEntryCount); err != nil {
		playlistEntryCount = 0
	}

	playlistEntry.Order = playlistEntryCount + 1
	if err := tx.Create(&playlistEntry).Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Commit().Error; err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusCreated, &playlistEntry)
	}
}

func (h *AdminHandler) patchPlaylistEntry(c *gin.Context) {
	type Direction string
	const (
		Up   Direction = "up"
		Down Direction = "down"
	)
	args := struct {
		Direction Direction `json:"direction" form:"direction" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	playlistEntries := []PlaylistEntry{}
	result := h.DB.
		Preload("Playlist").
		Preload("Entry").
		Order("`order` asc").
		Find(&playlistEntries, "playlist_id = ?", c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	index, playlistEntry := func() (int, PlaylistEntry) {
		entryID := cast.ToUint(c.Param("entry_id"))
		for i, entry := range playlistEntries {
			if entry.EntryID == entryID {
				return i, entry
			}
		}
		panic("entry not found")
	}()

	if reorderedPlaylistEntry := func() *PlaylistEntry {
		switch args.Direction {
		case Up:
			if index > 0 {
				return &playlistEntries[index-1]
			}
		case Down:
			if index < len(playlistEntries)-1 {
				return &playlistEntries[index+1]
			}
		}

		return nil
	}(); reorderedPlaylistEntry != nil {
		reorderedPlaylistEntry.Order, playlistEntry.Order = playlistEntry.Order, reorderedPlaylistEntry.Order
		tx := h.DB.Begin()
		tx.Save(&playlistEntry)
		tx.Save(&reorderedPlaylistEntry)
		result := tx.Commit()
		if result.Error != nil {
			panic(result.Error)
		}
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, playlistEntry.Playlist.Path()+"?"+c.Request.URL.RawQuery)
	} else {
		c.JSON(http.StatusOK, &playlistEntry)
	}
}

func (h *AdminHandler) getPlaylistEntries(c *gin.Context) {
	playlistEntries := []PlaylistEntry{}
	if err := h.DB.
		Preload("Playlist").
		Preload("Entry").
		Find(&playlistEntries, "playlist_id = ?", c.Param("playlist_id")).
		Error; err != nil {
		panic(err)
	}
	c.JSON(http.StatusOK, &playlistEntries)
}

func (h *AdminHandler) deletePlaylistEntry(c *gin.Context) {
	result := h.DB.
		Where("playlist_id = ? and entry_id = ?",
			c.Param("playlist_id"),
			c.Param("entry_id")).
		Delete(&PlaylistEntry{})
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (h *AdminHandler) getPlaylist(c *gin.Context) {
	playlist := Playlist{}
	result := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	compos := []Compo{}
	if result = h.DB.Preload("Entries.Uploads").Find(&compos); result.Error != nil {
		panic(result.Error)
	}

	selectedCompo := func() *Compo {
		cmp, err := strconv.ParseUint(c.Query("compo"), 10, 0)
		if err != nil {
			return nil
		}

		compoId := uint(cmp)

		for _, compo := range compos {
			if compo.ID == compoId {
				return &compo
			}
		}

		return nil
	}()

	data := gin.H{
		"Config":        &h.Config.Routing,
		"Title":         "Playlist",
		"Playlist":      &playlist,
		"Compos":        compos,
		"SelectedCompo": selectedCompo,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/playlist",
		HTMLData: &data,
		Data:     &playlist,
	})
}

func (h *AdminHandler) getPlaylistArchive(c *gin.Context) {
	// TODO: Combine this with getCompoArchive() somehow to avoid (this much)
	//       code duplication.
	query := struct {
		Ordered bool `form:"ordered"`
	}{}
	if err := c.ShouldBindQuery(&query); err != nil {
		panic(err)
	}

	playlist := Playlist{}
	if err := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/zip")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+slug.Make(playlist.Name)+".zip")
	// TODO: Test this. For some reason, invoking c.Stream() causes the following
	//       error in the test suite (but not in the browser):
	//       interface conversion: *httptest.ResponseRecorder is not http.CloseNotifier:
	//       missing method CloseNotify
	c.Stream(func(w io.Writer) bool {
		playlistArchive := zip.NewWriter(w)
		defer playlistArchive.Close()

		for _, entry := range playlist.Entries {
			if len(entry.Entry.Uploads) == 0 {
				// If there are no uploads, skip this entry.
				continue
			}

			if !query.Ordered && len(entry.Entry.Uploads) == 1 {
				// If we don't want an ordered archive, and if there's only one
				// upload, don't create a zip file for it.
				upload := entry.Entry.Uploads[0]
				f, _ := playlistArchive.Create(upload.Filename)
				f.Write(upload.Content)
				continue
			}

			// Otherwise, create a zip file that contains all uploads for the entry.
			entryBuffer := &bytes.Buffer{}
			entryArchive := zip.NewWriter(entryBuffer)
			defer entryArchive.Close()

			entryFilename := fmt.Sprintf("%s-%s", slug.Make(entry.Entry.Author), slug.Make(entry.Entry.Title))

			if query.Ordered {
				entryFilename = fmt.Sprintf("%02d_%s", entry.Order, entryFilename)
			}

			for _, upload := range entry.Entry.Uploads {
				filename := fmt.Sprintf("%s/%s", entryFilename, upload.Filename)
				f, _ := entryArchive.Create(filename)
				f.Write(upload.Content)
			}

			f, _ := playlistArchive.Create(entryFilename + ".zip")
			f.Write(entryBuffer.Bytes())
		}

		return false
	})
}

func (h *AdminHandler) getCompoArchive(c *gin.Context) {
	// TODO: Combine this with getPlaylistArchive() somehow to avoid (this much)
	//       code duplication.
	compo := Compo{}
	if err := h.DB.
		Preload("Entries").
		Preload("Entries.Uploads").
		First(&compo, c.Param("compo_id")).
		Error; err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/zip")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+slug.Make(compo.Name)+".zip")
	// TODO: Test this. For some reason, invoking c.Stream() causes the following
	//       error in the test suite (but not in the browser):
	//       interface conversion: *httptest.ResponseRecorder is not http.CloseNotifier:
	//       missing method CloseNotify
	c.Stream(func(w io.Writer) bool {
		compoArchive := zip.NewWriter(w)
		defer compoArchive.Close()

		for _, entry := range compo.Entries {
			if len(entry.Uploads) == 0 {
				// If there are no uploads, skip this entry.
				continue
			}

			if len(entry.Uploads) == 1 {
				// If there's only one upload, don't create a zip file for it.
				upload := entry.Uploads[0]
				f, _ := compoArchive.Create(upload.Filename)
				f.Write(upload.Content)
				continue
			}

			// Otherwise, create a zip file that contains all uploads for the entry.
			entryBuffer := &bytes.Buffer{}
			entryArchive := zip.NewWriter(entryBuffer)
			defer entryArchive.Close()

			entryFilename := fmt.Sprintf("%s-%s", slug.Make(entry.Author), slug.Make(entry.Title))
			for _, upload := range entry.Uploads {
				filename := fmt.Sprintf("%s/%s", entryFilename, upload.Filename)
				f, _ := entryArchive.Create(filename)
				f.Write(upload.Content)
			}

			f, _ := compoArchive.Create(entryFilename + ".zip")
			f.Write(entryBuffer.Bytes())
		}

		return false
	})
}

func (h *AdminHandler) deletePlaylist(c *gin.Context) {
	result := h.DB.Delete(&Playlist{}, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (h *AdminHandler) getPlaylistPreview(c *gin.Context) {
	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistPreview := PlaylistPreview{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}
	data := gin.H{
		"Config":  &h.Config.Routing,
		"Title":   "Preview Playlist",
		"Preview": &playlistPreview,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/playlist-preview",
		HTMLData: &data,
		Data:     &playlistPreview,
	})
}

func (h *AdminHandler) patchPlaylist(c *gin.Context) {
	type PlaylistAction string
	const (
		Show  PlaylistAction = "show"
		Close PlaylistAction = "close"
		Open  PlaylistAction = "open"
	)
	args := struct {
		Action PlaylistAction `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	playlist := Playlist{}
	tx := h.DB.Begin()
	if err := tx.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Compo").
		First(&playlist, c.Param("playlist_id")).
		Error; err != nil {
		tx.Rollback()
		panic(err)
	}

	playlistShow, redirectUrl, err := func() (*PlaylistShow, string, error) {
		switch args.Action {
		case Show:
			playlistShow, err := playlist.Show(tx)
			return playlistShow, playlistShow.Path(), err
		case Close:
			playlistShow, err := playlist.Close(tx)
			return playlistShow, playlistShow.Playlist.Path(), err
		case Open:
			playlistShow, err := playlist.Open(tx)
			return playlistShow, playlistShow.Playlist.Path(), err
		default:
			return nil, "", fmt.Errorf("invalid action: %v", args.Action)
		}
	}()

	if err != nil {
		tx.Rollback()
		panic(err)
	}

	if err := tx.Commit().Error; err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, redirectUrl)
	} else {
		c.JSON(http.StatusOK, playlistShow)
	}
}

func (h *AdminHandler) getPlaylistShow(c *gin.Context) {
	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistShow := PlaylistShow{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}
	data := gin.H{
		"Config": &h.Config.Routing,
		"Title":  "Show Playlist",
		"Show":   &playlistShow,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/playlist-show",
		HTMLData: &data,
		Data:     &playlistShow,
	})
}

func (h *AdminHandler) postPlaylistShowSlide(c *gin.Context) {
	type PlaylistAction string
	const (
		Show PlaylistAction = "show"
	)
	args := struct {
		Action PlaylistAction `form:"action" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	if args.Action != Show {
		panic(fmt.Errorf("invalid action: %v", args.Action))
	}

	playlist, slideNumber := h.getPlaylistAndSlideNumber(c)
	playlistEntry := playlist.Entries[slideNumber-1]
	if err := playlistEntry.Show(h.DB); err != nil {
		panic(err)
	}

	playlistShow := PlaylistShow{
		Playlist:    playlist,
		SlideNumber: slideNumber,
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, playlistShow.Path())
	} else {
		c.JSON(http.StatusOK, &playlistShow)
	}
}

func (h *AdminHandler) getPlaylistAndSlideNumber(c *gin.Context) (Playlist, int) {
	playlist := Playlist{}
	result := h.DB.
		Preload("Entries", func(db *gorm.DB) *gorm.DB {
			return db.Order("`order` asc")
		}).
		Preload("Entries.Entry.Uploads.Entry").
		Preload("Entries.Playlist").
		First(&playlist, c.Param("playlist_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	slideNumber := func() int {
		sn, err := strconv.ParseUint(c.Param("slide_number"), 10, 0)
		if err != nil {
			return 1
		}
		slideNumber := int(sn)
		if slideNumber < 1 {
			return 1
		} else if slideNumber > len(playlist.Entries) {
			return len(playlist.Entries)
		}
		return slideNumber
	}()

	return playlist, slideNumber
}

func (h *AdminHandler) mapPlaylistEntry(db *gorm.DB, entryID interface{}, playlist Playlist) (*PlaylistEntry, error) {
	entry := Entry{}

	// A foreign constraint should check this, but I can't seem to get that to work with SQLite.
	if err := db.First(&entry, entryID).Error; err != nil {
		return nil, err
	}

	return &PlaylistEntry{
		Entry:      entry,
		EntryID:    entry.ID,
		Playlist:   playlist,
		PlaylistID: playlist.ID,
	}, nil
}

func (h *AdminHandler) registerRoutes(r *gin.Engine, admins map[string]string) {
	admin := r.Group("", gin.BasicAuth(admins))
	admin.GET("", h.showAdmin)

	invites := admin.Group("/invites")
	invites.GET("", h.getInvites)
	invites.POST("", h.postInvite)

	users := admin.Group("/users")
	users.GET("", h.getUsers)
	users.POST("", h.postUser)

	user := users.Group("/:user_id")
	user.GET("", h.getUser)
	user.PUT("", h.putUser)
	user.DELETE("", h.deleteUser)

	compos := admin.Group("/compos")
	compos.GET("", h.getCompos)
	compos.POST("", h.postCompo)

	compo := compos.Group("/:compo_id")
	compo.GET("", h.getCompo)
	compo.GET("/archive", h.getCompoArchive)
	compo.PUT("", h.putCompo)
	compo.DELETE("", h.deleteCompo)
	compo.POST("/entries", h.postCompoEntry)

	entries := admin.Group("/entries")
	entries.GET("", h.getEntries)

	entry := entries.Group("/:entry_id")
	entry.GET("", h.getEntry)
	entry.GET("/preview", h.getEntryPreview)
	entry.PUT("", h.putEntry)
	entry.DELETE("", h.deleteEntry)

	uploads := entry.Group("/uploads")
	uploads.GET("", h.getUploads)
	uploads.POST("", h.postUpload)

	upload := uploads.Group("/:upload_id")
	upload.GET("", h.getUpload)
	upload.DELETE("", h.deleteUpload)
	upload.GET("/data", h.getUploadData)

	playlists := admin.Group("/playlists")
	playlists.GET("", h.getPlaylists)
	playlists.PATCH("", h.patchPlaylists)
	playlists.POST("", h.postPlaylist)

	playlist := playlists.Group("/:playlist_id")
	playlist.GET("", h.getPlaylist)
	playlist.GET("/archive", h.getPlaylistArchive)
	playlist.PUT("", h.putPlaylist)
	playlist.PATCH("", h.patchPlaylist)
	playlist.DELETE("", h.deletePlaylist)

	playlistEntries := playlist.Group("/entries")
	playlistEntries.GET("", h.getPlaylistEntries)
	playlistEntries.POST("", h.postPlaylistEntry)

	playlistEntry := playlistEntries.Group("/:entry_id")
	playlistEntry.GET("", h.getPlaylistEntry)
	playlistEntry.PATCH("", h.patchPlaylistEntry)
	playlistEntry.DELETE("", h.deletePlaylistEntry)

	playlistPreview := playlist.Group("/preview")
	playlistPreview.GET("", h.getPlaylistPreview)
	playlistPreview.GET("/:slide_number", h.getPlaylistPreview)

	playlistShow := playlist.Group("/show")
	playlistShow.GET("", h.getPlaylistShow)

	playlistShowSlide := playlistShow.Group("/:slide_number")
	playlistShowSlide.GET("", h.getPlaylistShow)
	playlistShowSlide.POST("", h.postPlaylistShowSlide)
}
