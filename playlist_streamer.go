package main

import (
	"granola/models"
)

type PlaylistStreamChan chan *models.PlaylistEntry

type PlaylistStreamer struct {
	CurrentEntry  PlaylistStreamChan
	NewClient     chan PlaylistStreamChan
	ClosedClient  chan PlaylistStreamChan
	ActiveClients map[PlaylistStreamChan]struct{}
}

func NewPlaylistStreamer() PlaylistStreamer {
	return PlaylistStreamer{
		CurrentEntry:  make(PlaylistStreamChan),
		NewClient:     make(chan PlaylistStreamChan),
		ClosedClient:  make(chan PlaylistStreamChan),
		ActiveClients: make(map[PlaylistStreamChan]struct{}),
	}
}

func (streamer *PlaylistStreamer) newClient(clientChan PlaylistStreamChan) {
	streamer.ActiveClients[clientChan] = struct{}{}
}

func (streamer *PlaylistStreamer) closeClient(clientChan PlaylistStreamChan) {
	delete(streamer.ActiveClients, clientChan)
	close(clientChan)
}

func (streamer *PlaylistStreamer) broadcast(currentEntry *models.PlaylistEntry) {
	for clientMessageChan := range streamer.ActiveClients {
		clientMessageChan <- currentEntry
	}
}
