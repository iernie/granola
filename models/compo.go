package models

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Compo struct {
	gorm.Model
	Name          string
	MultiPlatform bool
	Locked        bool
	Entries       []Entry
}

func (compo *Compo) Path() string {
	return fmt.Sprintf("/compos/%d", compo.ID)
}

func (compo *Compo) ArchivePath() string {
	return fmt.Sprintf("%v/archive", compo.Path())
}

func NewCompo(db *gorm.DB, name string, multiPlatform bool, locked bool) (Compo, error) {
	compo := Compo{
		Name:          name,
		MultiPlatform: multiPlatform,
		Locked:        locked,
	}
	result := db.Create(&compo)
	return compo, result.Error
}

func (compo *Compo) MarshalJSON() ([]byte, error) {
	return json.Marshal(gin.H{
		"id":            compo.Path(),
		"name":          compo.Name,
		"multiPlatform": compo.MultiPlatform,
		"locked":        compo.Locked,
	})
}
