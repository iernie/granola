package models

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Upload struct {
	gorm.Model
	EntryID     uint
	Entry       Entry
	Filename    string
	Content     []byte
	ContentSize int
	ContentType string
}

func NewUpload(db *gorm.DB, filename string, content []byte, entry Entry) (Upload, error) {
	upload := Upload{
		Filename:    filename,
		Content:     content,
		ContentType: http.DetectContentType(content),
		ContentSize: len(content),
		Entry:       entry,
	}

	result := db.Create(&upload)
	return upload, result.Error
}

func (upload *Upload) IsImage() bool {
	return strings.HasPrefix(upload.ContentType, "image/")
}

func (upload *Upload) Path() string {
	return fmt.Sprintf("%v/uploads/%d", upload.Entry.Path(), upload.ID)
}

func (upload *Upload) MarshalJSON() ([]byte, error) {
	return json.Marshal(gin.H{
		"id":          upload.Path(),
		"entry":       upload.Entry.Path(),
		"filename":    upload.Filename,
		"contentType": upload.ContentType,
	})
}
