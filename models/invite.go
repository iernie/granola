package models

import (
	"database/sql"
	"encoding/json"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Invite struct {
	gorm.Model
	Key    string        `gorm:"uniqueIndex:idx_uq_invites_key"`
	UserID sql.NullInt64 `gorm:"uniqueIndex:idx_uq_invites_userid"`
	User   User
}

func NewInvite(db *gorm.DB, key string) (Invite, error) {
	invite := Invite{
		Key: key,
	}
	result := db.Create(&invite)
	return invite, result.Error
}

func (invite *Invite) MarshalJSON() ([]byte, error) {
	return json.Marshal(gin.H{
		"key": invite.Key,
	})
}
