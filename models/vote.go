package models

import (
	"encoding/json"

	"github.com/dustin/go-humanize"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Vote struct {
	gorm.Model
	EntryID uint
	Entry   Entry
	UserID  uint
	User    User
	Score   uint `gorm:"not null;default:0"`
}

type VotingPlaylist struct {
	Playlist
	Entries []VotingEntry
}

type VotingEntry struct {
	PlaylistEntry
	IsCurrent bool
	Vote      *Vote
}

func (vote *Vote) AsMap() gin.H {
	return gin.H{
		"entry": vote.Entry.Path(),
		"user":  vote.User.Path(),
		"score": vote.Score,
		"created": gin.H{
			"dateTime":  vote.CreatedAt,
			"humanized": humanize.Time(vote.CreatedAt),
		},
	}
}

func (vote *Vote) MarshalJSON() ([]byte, error) {
	return json.Marshal(vote.AsMap())
}

func (votingEntry *VotingEntry) MarshalJSON() ([]byte, error) {
	h := votingEntry.PlaylistEntry.AsMap()

	if votingEntry.Vote != nil {
		h["vote"] = votingEntry.Vote.AsMap()
	}

	return json.Marshal(h)
}
