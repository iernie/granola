package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"

	"github.com/gin-gonic/gin"
	"github.com/yuin/goldmark"
	"gorm.io/gorm"
)

type Entry struct {
	gorm.Model
	Title    string
	Author   string
	Platform string
	Notes    string
	CompoID  uint
	UserID   uint
	User     User
	Compo    Compo
	Locked   bool
	Uploads  []Upload
}

func (entry *Entry) Path() string {
	return fmt.Sprintf("/entries/%d", entry.ID)
}

func (entry *Entry) PreviewPath() string {
	return fmt.Sprintf("%v/preview", entry.Path())
}

func (entry *Entry) NotesHtml() template.HTML {
	if entry.Notes == "" {
		return ""
	}

	md := []byte(entry.Notes)
	var b bytes.Buffer
	if err := goldmark.Convert(md, &b); err != nil {
		panic(err)
	}

	return template.HTML(b.Bytes())
}

func (entry *Entry) AsMap() gin.H {
	h := gin.H{
		"id":      entry.Path(),
		"title":   entry.Title,
		"author":  entry.Author,
		"locked":  entry.Locked,
		"preview": gin.H{"id": entry.PreviewPath()},
	}

	if entry.Notes != "" {
		h["notes"] = entry.Notes
	}

	if entry.Platform != "" {
		h["platform"] = entry.Platform
	}

	if entry.Compo.ID != 0 {
		h["compo"] = entry.Compo.Path()
	}

	if entry.User.ID != 0 {
		h["user"] = entry.User.Path()
	}

	return h
}

func (entry *Entry) MarshalJSON() ([]byte, error) {
	h := entry.AsMap()
	return json.Marshal(h)
}
