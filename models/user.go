package models

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Email        string `gorm:"default:null;not null;uniqueIndex:idx_uq_users_email"`
	Handle       string
	PasswordHash string
}

func NewUser(db *gorm.DB, email string, handle string, password string, invite string) (User, error) {
	user := User{Email: email, Handle: handle}
	if err := user.SetPassword(password); err != nil {
		return user, err
	}

	tx := db.Begin()
	if err := tx.Create(&user).Error; err != nil {
		tx.Rollback()
		return user, err
	}

	result := tx.Model(&Invite{}).Where("key = ? AND user_id is null", invite).Update("UserID", user.ID)
	if result.Error != nil || result.RowsAffected == 0 {
		tx.Rollback()
		return user, errors.New("invalid invitation")
	}

	if err := tx.Commit().Error; err != nil {
		return user, err
	}

	return user, nil
}

func (user *User) Path() string {
	return fmt.Sprintf("/users/%d", user.ID)
}

func (user *User) SetPassword(plainTextPassword string) error {
	password := []byte(plainTextPassword)

	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user.PasswordHash = string(hashedPassword)

	return nil
}

func (user *User) CheckPassword(plainTextPassword string) bool {
	if user.PasswordHash == "" {
		return false
	}

	password := []byte(plainTextPassword)
	hashedPassword := []byte(user.PasswordHash)
	err := bcrypt.CompareHashAndPassword(hashedPassword, password)
	return err == nil
}

func (user *User) MarshalJSON() ([]byte, error) {
	return json.Marshal(gin.H{
		"id":     user.Path(),
		"email":  user.Email,
		"handle": user.Handle,
	})
}

func (user *User) Votes(db *gorm.DB, entries []uint) ([]*Vote, error) {
	if user == nil {
		return nil, nil
	}

	var votes []*Vote
	err := db.
		Preload("Entry").
		Preload("User").
		Where("user_id = ?", user.ID).
		Where("entry_id in (?)", entries).
		Group("entry_id").
		Having("created_at = max(created_at)").
		Find(&votes).
		Error

	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	return votes, err
}
